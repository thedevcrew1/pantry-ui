//
//  AddItemsObservable.swift
//  Pantry
//
//  Created by Carson Franklin on 6/17/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

class AddItemsObservable: ObservableObject {
    @Published private(set) var items: [PantryItem] = []
    @Published var mode: Mode = .display
    @Published var sheetShown: Bool = false
    @Published var selectedCategory: ProductCategory = ProductCategory(id: UUID().uuidString, value: "NA")
    
    func add(item: PantryItem) -> Void {
        items.append(item)
    }
    
    func remove(item: PantryItem) -> Void {
        items = items.filter({ itemInList in
            return itemInList.id != item.id
        })
    }
    
    func displayNewProductSheet() -> Void {
        mode = .newProductSheetShown
        sheetShown = true
    }
    
    
    func isSearchProductsChanged(_ isSearching: Bool) {
        withAnimation {
            mode = isSearching ? .searchingProducts : .display
        }
    }
    
    func categoryTileClicked(_ category: ProductCategory) {
        withAnimation {
            mode = .viewCategory
            selectedCategory = category
        }
    }
    
    func scanItemsBtnClicked() -> Void {
        withAnimation {
            mode = .scanItems
        }
    }
    
    func addItemsBtnClicked() -> Void {
        withAnimation {
            sheetShown = true
        }
    }
    
    func itemsConfirmed() -> Void {
        withAnimation {
            NavigationObserver.shared.navigateToPrevious()
        }
    }
    
    func sheetDissmissed() -> Void {
        mode = .display
        sheetShown = false
    }
    
    enum Mode {
        case display
        case searchingProducts
        case viewCategory
        case searchingCategory
        case scanItems
        case confirmAdd
        case newProductSheetShown
    }
}
