//
//  ScanItemsObservable.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

class ScanItemsObservable: ObservableObject {
    @Published var scannedItems: [PantryItem] = []
    @Published var scannedUPCs: Set<String> = []
    @Published var sheetShown: Bool = false {
        didSet {
            if sheetShown {
                newProductSheetDisplayed()
            }
        }
    }
    @Published var mode: Mode = .display
    @Published var newProductSheetDisplayed: Bool = false
    @ObservedObject var newProductObservable: NewProductObservable = NewProductObservable(product: Product(id: UUID().uuidString, name: "", category: ProductCategory(id: UUID().uuidString, value: ""), experiaitonPeriod: ""))
    @ObservedObject var addItemsObservable: AddItemsObservable
    
    let newProductSheetDisplayed: () -> Void
    
    init(addItemsObservable: AddItemsObservable, newProductSheetDisplayed: @escaping () -> Void) {
        self.addItemsObservable = addItemsObservable
        self.newProductSheetDisplayed = newProductSheetDisplayed
    }
    
    func delete(_ item: PantryItem){
        
        withAnimation {
            scannedItems = scannedItems.filter({$0.id != item.id})
            addItemsObservable.remove(item: item)
        }
    }
    
    func inCodes(value: String) -> Bool {
        for item in scannedItems {
            if item.id == value { return true}
        }
        
        return false
    }
    
    func scanCompleted(_ result: Result<String, CodeScannerView.ScanError>){
        withAnimation {
            switch result {
            case .success(let scannedUPC):
                if !scannedUPCs.contains(scannedUPC) {
                    scannedUPCs.insert(scannedUPC)
                    mode = .loading
                    
                    let service = UPCLookupService()
                    service.execute(UPCLookupRequest(code: scannedUPC), onCompleteHandler: onUPCLookupComplete)
                }
                break
                
            case .failure(let e) :
                print("There was an error scanning a barcode:\n\(e)")
                appNotificationObservable.show(text: "Sorry there was a problem scanning that barcode. Please try again.", mode: .error)
            }
        }
    }
    
    private func onUPCLookupComplete(_ result: Result<UPCLookupResponse, NetworkError>) -> Void {
        DispatchQueue.main.async {
            switch result {
            case .success(let response):
                if response.newProduct {
                    self.newProductDetected(name: response.name ?? "")
                } else {
                    guard let product = response.product else {
                        appNotificationObservable.show(text: "Sorry there was a problem adding this item. Please try again later.", mode: .error)
                        print("The response form looking up that product said it wasn't a new product, but it didn't return a product to us.")
                        self.mode = .display
                        return
                    }
                    
                    self.addItemFromProduct(product: product)
                }
                
            case .failure(let e):
                print("There was an error looking up a UPC.\n\(e)")
                appNotificationObservable.show(text: "Sorry there was an error looking up that barcode. Please try again.", mode: .error)
            }
            
            self.mode = .display
        }
    }
    
    private func newProductDetected(name: String) {
        newProductObservable.product.name = name
        newProductSheetDisplayed = true
    }
    
    private func addItemFromProduct(product: Product) {
        let expirationDate = calcExpirationDate(expirationPeriod: product.experiaitonPeriod)
        
        let newItem = PantryItem(id: UUID().uuidString, expirationDate: expirationDate, state: .defualt, product: product)
        
        scannedItems.append(newItem)
        addItemsObservable.add(item: newItem)
    }
    
    enum Mode {
        case display
        case loading
    }
}
