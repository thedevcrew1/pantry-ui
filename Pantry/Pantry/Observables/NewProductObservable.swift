//
//  NewProductObservable.swift
//  Pantry
//
//  Created by Carson Franklin on 7/15/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

class NewProductObservable: ObservableObject {
    @Published var product: Product
    @Published var selectedProductIndex = 0
    @Published var intervalQuantity = 5
    @Published var intervalUnitIndex = 0
    
    init(product: Product) {
        self.product = product
    }
    
    func resetValues() {
        product = Product(id: UUID().uuidString, name: "", category: ProductCategory(id: UUID().uuidString, value: ""), experiaitonPeriod: "")
        selectedProductIndex = 0
        intervalQuantity = 5
        intervalUnitIndex = 0
    }
    
    func save() -> Void {
        
    }
}
