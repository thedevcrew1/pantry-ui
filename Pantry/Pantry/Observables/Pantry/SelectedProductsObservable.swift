//
//  SelectedProductsObservable.swift
//  Pantry
//
//  Created by Carson Franklin on 5/13/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

class SelectedProductsObservable: ObservableObject {
    
    @Published private(set) var products: [Product] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }
    
    func remove(_ product: Product) -> Void {
        withAnimation {
            products = products.filter({selectedProduct in
                return selectedProduct.id != product.id  
            })
        }
    }
    
    func append(_ product: Product) -> Void {
        withAnimation {
            products.append(product)
        }
    }
    
    func clear() -> Void {
        products = []
    }
}
