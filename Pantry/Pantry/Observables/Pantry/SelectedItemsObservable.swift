//
//  SelectedItemsObservable.swift
//  Pantry
//
//  Created by Carson Franklin on 5/13/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

class SelectedItemsObservable: ObservableObject {
    
    @Published var items: [PantryItem] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }
    
    func remove(_ item: PantryItem){
        items = items.filter({selectedItem in
            return selectedItem.id != item.id
            
        })
    }
}
