//
//  NotificationObservable.swift
//  Pantry
//
//  Created by Carson Franklin on 7/14/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

class NotificationObservable: ObservableObject {
    
    @Published private(set) var title: String = ""
    @Published private(set) var text: String = ""
    @Published var mode: Mode = .hidden
    @Published var preventDissmiss = false {
        didSet {
            if !preventDissmiss && timerHasFinished {
                mode = .hidden
            }
        }
    }
    
    var timerHasFinished = false
    
    
    func show(text: String, mode: Mode, time: Int = 10) -> Void {
        self.text = text
        self.mode = mode
        
        dismissAfterTime(time: time)
    }
    
    func show(title: String, text: String, mode: Mode,  time: Int = 10) -> Void {
        self.title = title
        self.show(text: text, mode: mode)
        
        dismissAfterTime(time: time)
    }
    
    func resetVals() {
        mode = .hidden
        title = ""
        text = ""
    }
    
    func dismissAfterTime(time: Int) {
        DispatchQueue.global(qos: .utility).async {
            sleep(UInt32(time))
            
            DispatchQueue.main.async {
                if self.preventDissmiss {
                    self.timerHasFinished = true
                } else {
                    self.resetVals()
                }
            }
        }
    }
    
    enum Mode {
        case success
        case error
        case info
        case hidden
    }
}

let appNotificationObservable = NotificationObservable()
