//
//  Store.swift
//  Pantry
//
//  Created by Carson Franklin on 4/25/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

// IMPORTANT: This is a SINGLETON class.
class Store: ObservableObject {
    
    static var shared = Store()
    
    @Published var user: User?
    
    
    private var nf = NetworkFacade()
    
    init() {
    }
}
