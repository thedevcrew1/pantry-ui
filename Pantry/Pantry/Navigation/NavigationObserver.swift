//
//  NavigationObserver.swift
//  Pantry
//
//  Created by Carson Franklin on 5/2/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

// The purpose of this class is to track the navigation of the application. I am
// creating this because it seems the purposes of the NavigationView and
// NavigationLink provided by Apple were creating with slightly different intents.
// However, the difference is very apparent in behavior. Hence the creation of this
// class.


// IMPORTANT: This class is a SINGLETON.
class NavigationObserver: ObservableObject {
    
    static var shared = NavigationObserver()
    
    @Published private(set) var curView: CustomNavigationView! = nil
    @Published private(set) var showNext: Bool = true
    @Published private(set) var showPrev: Bool = false
    
    // The purpose of this stack is to keep track of context. Mainly for implimenting
    // a back button.
    private var viewStack: [CustomNavigationView] = []
    
    func initView(firstView: CustomNavigationView) {
        self.curView = firstView
    }
    
    func navigateTo(destination: CustomNavigationView) -> Void {
        withAnimation {
            if self.curView != nil {
                self.viewStack.append(self.curView)
            }
            var destinationCopy = destination
            destinationCopy.mode = CustomNavigationView.Mode.navigateForward
            self.curView = destinationCopy
        }
    }
    
    func navigateToPrevious() {
        withAnimation {
            self.curView.mode = CustomNavigationView.Mode.navigateBack
            var curView = viewStack.popLast()
            curView?.mode = CustomNavigationView.Mode.navigateBack
            self.curView = curView
        }
    }
    
    func navigateToRoot() -> Void {
        if self.viewStack.count < 1 {
            return
        }
        
        withAnimation {
            self.curView = self.viewStack[0]
            self.viewStack = []
        }
    }
}


struct ExceptionNavigationView: View {
    var body: some View {
        VStack {
            Text("Sorry, something wrong has occured with navigation. Please try again later")
            Spacer()
        }
    }
}
