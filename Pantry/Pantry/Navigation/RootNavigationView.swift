//
//  RootNavigationView.swift
//  Pantry
//
//  Created by Carson Franklin on 5/2/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct RootNavigationView: View {
    
    @ObservedObject var navigationObserver = NavigationObserver.shared
    
    let arrayOfViews = [
        Text("View 1"),
        Text("View 2"),
        Text("View 3")
    ]
    
    
    init(firstView: ContentView) {
        navigationObserver.initView(firstView: CustomNavigationView(AnyView(firstView), backButtonHidden: true))
    }
    
    var body: some View {
        ZStack {
            navigationObserver.curView
                .transition(.slide)
        }
    }
    
    struct TestView: View {
        let number: Int
        let clicked: () -> Void
        
        var body: some View {
            VStack {
                Text("View \(number)")
            }
            .onTapGesture {
                self.clicked()
            }
        }
    }
}
