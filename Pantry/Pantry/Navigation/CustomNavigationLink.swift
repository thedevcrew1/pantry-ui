//
//  CustomNavigationLink.swift
//  Pantry
//
//  Created by Carson Franklin on 5/2/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

class NavigationLinkObservable<Destination>: ObservableObject where Destination: View {
    @ObservedObject private var navigationObserver = NavigationObserver.shared
    private var destionation: Destination?
    private var backgroundMode: BackgroundImage.Mode?
    private var backButtonColor: Color?
    private var hideBackButton: Bool = false
    
    init() {
    }
    
    init(destination: Destination) {
        self.destionation = destination
    }
    
    init(destination: Destination, hideBackButton: Bool) {
        self.destionation = destination
        self.hideBackButton = hideBackButton
    }
    
    init(destination: Destination, background: BackgroundImage.Mode, backButtonColor: Color) {
        self.destionation = destination
        self.backgroundMode = background
        self.backButtonColor = backButtonColor
    }
    
    func setDestination(destination: Destination){
        self.destionation = destination
    }
    
    func navigate(){
        if type(of: self.destionation!) == Pantry.CustomNavigationView.self {
            navigationObserver.navigateTo(destination: self.destionation as! CustomNavigationView)
            
        } else if backgroundMode != nil && backButtonColor != nil {
            navigationObserver.navigateTo(destination: CustomNavigationView(AnyView(self.destionation), background: self.backgroundMode!, backButtonColor: backButtonColor!))
            
        } else if hideBackButton {
            navigationObserver.navigateTo(destination: CustomNavigationView(backButtonHidden: true, genericView: AnyView(self.destionation)))
            
        } else {
            navigationObserver.navigateTo(destination: CustomNavigationView(AnyView(self.destionation)))
            
        }
    }
}

struct CustomNavigationLink<Label, Destination> : View where Label: View, Destination: View {
    
    @ObservedObject private var navigationObserver = NavigationObserver.shared
    @ObservedObject private var navigationLinkObservable: NavigationLinkObservable<Destination>
    private var label: Label
    
    public init(destination: Destination, @ViewBuilder label: @escaping () -> Label){
        self.navigationLinkObservable = NavigationLinkObservable(destination: destination)
        self.label = label()
    }
    
    public init(destination: Destination, hideBackButton: Bool, @ViewBuilder label: @escaping() -> Label) {
        self.label = label()
        self.navigationLinkObservable = NavigationLinkObservable(destination: destination, hideBackButton: hideBackButton)
    }
    
    public init(destination: Destination, background: BackgroundImage.Mode, backButtonColor: Color, @ViewBuilder label: @escaping () -> Label){
        self.navigationLinkObservable = NavigationLinkObservable(destination: destination, background: background, backButtonColor: backButtonColor)
        self.label = label()
    }
    
    // MARK: Progrommatic Navigation
    /// To progrommatically navigate, pass in a NavigationLinkObservable object. Then, when you want to navigate call the navigat() function.
    public init(destination: Destination, navigationLinkObservable: NavigationLinkObservable<Destination>, @ViewBuilder label: @escaping () -> Label){
        self.navigationLinkObservable = navigationLinkObservable
        self.label = label()
        self.navigationLinkObservable.setDestination(destination: destination)
    }
    
    var body: some View {
        Button(action: { self.navigationLinkObservable.navigate() }){
            self.label
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct CustomNavigationLink_Previews: PreviewProvider {
    static var previews: some View {
        
        CustomNavigationLink(destination: ExceptionNavigationView(), label: {
            Text("foo")
        })
    }
}
