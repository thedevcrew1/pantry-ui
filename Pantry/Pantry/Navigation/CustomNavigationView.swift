//
//  CustomNavigationView.swift
//  Pantry
//
//  Created by Carson Franklin on 5/2/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct CustomNavigationView: View {
    
    private var navigationObserver = NavigationObserver.shared
    private var genericView: AnyView
    private var backButtonHidden = false
    var mode: Mode = Mode.navigateForward
    private var backGroundMode: BackgroundImage.Mode?
    private var backButtonColor: Color?
    
    init(@ViewBuilder genericView: @escaping () -> AnyView) {
        self.genericView = genericView()
    }
    
    init(_ generic: AnyView) {
        self.genericView = AnyView(generic)
    }
    
    init(_ generic: AnyView, background: BackgroundImage.Mode, backButtonColor: Color) {
        self.genericView = AnyView(generic)
        self.backGroundMode = background
        self.backButtonColor = backButtonColor
    }
    
    init(backButtonHidden: Bool, @ViewBuilder genericView: @escaping () -> AnyView) {
        self.genericView = genericView()
        self.backButtonHidden = backButtonHidden
    }
    
    init(backButtonHidden: Bool, genericView: AnyView) {
        self.genericView = genericView
        self.backButtonHidden = backButtonHidden
    }
    
    init(_ generic: AnyView, backButtonHidden: Bool) {
        self.genericView = AnyView(generic)
        self.backButtonHidden = backButtonHidden
    }
    
    
    var body: some View {
        ZStack {
            if backGroundMode != nil {
                BackgroundImage(mode: self.backGroundMode!)
                .transition(mode == Mode.navigateForward ? .asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading)) : .asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
            }
            VStack {
                if !backButtonHidden {
                    HStack {
                        Button(action: {
                            self.navigationObserver.navigateToPrevious()
                        }){
                            HStack {
                                Image(systemName: "chevron.left")
                                    .imageScale(.medium)
                                    .foregroundColor(self.backButtonColor ?? Color.primary)
                                Text("Back")
                                    .foregroundColor(self.backButtonColor ?? Color.primary)
                            }
                        }.padding(.top)
                        Spacer()
                    }.padding(.leading)
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .transition(.move(edge: .trailing))
                }
                
                // This condition is here, because without it SwiftUI doesn't recognize the view as having the possiblity of being added/ deleted from
                // the view hierarchy and therefore won't apply the transition.
                if true {
                    self.genericView
                        .transition(mode == Mode.navigateForward ? .asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading)) : .asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
                }
            }
        }
    }
    
    enum Mode {
        case navigateForward
        case navigateBack
    }
}

struct CustomNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        CustomNavigationView(AnyView(ExceptionNavigationView()))
    }
}
