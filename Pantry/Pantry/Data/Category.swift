//
//  Category.swift
//  Pantry
//
//  Created by Carson Franklin on 5/7/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ProductCategory: Identifiable, Hashable, Encodable {
    var id: String
    var value: String
    
    init(id: String, value: String) {
        self.id = id
        self.value = value
    }
    
    init(json: JSON) throws {
        guard let id = json["id"].string else {
            throw NetworkError.malformedResponse
        }
        
        guard let value = json["value"].string else {
            throw NetworkError.malformedResponse
        }
        
        self.id = id
        self.value = value
    }
}
