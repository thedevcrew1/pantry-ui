//
//  IntervalUnits.swift
//  Pantry
//
//  Created by Carson Franklin on 7/15/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation

enum IntervalUnits: String, CaseIterable {
    case days = "Days"
    case months = "Months"
    case years = "Years"
}
