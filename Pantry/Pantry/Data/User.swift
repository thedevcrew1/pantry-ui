//
//  User.swift
//  Pantry
//
//  Created by Carson Franklin on 4/25/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation


struct User: Encodable {
    var firstName: String
    var lastName: String
    var username: String
    var email: String
    var password: String
}
