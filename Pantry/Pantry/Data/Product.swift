//
//  Product.swift
//  Pantry
//
//  Created by Carson Franklin on 5/7/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation

struct Product: Identifiable {
    var id: String
    var name: String
    var category: ProductCategory
    var experiaitonPeriod: String
    var items: [PantryItem] = []
    var code: String?
}
