//
//  PantryItem.swift
//  Pantry
//
//  Created by Carson Franklin on 5/7/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftyJSON

struct PantryItem: Identifiable {
    var id: String
    var expirationDate: Date
    var state: PantryItemState
    var product: Product?
    
    init(id: String, expirationDate: Date, state: PantryItemState, product: Product?) {
        self.id = id
        self.expirationDate = expirationDate
        self.state = state
        self.product = product
    }
    
    init(id: String, expirationDate: Date, state: PantryItemState) {
        self.id = id
        self.expirationDate = expirationDate
        self.state = state
    }
    
    init(id: String, expirationDate: Date, state: String, product: Product?) {
        var formattedState: PantryItemState!
        switch state {
        case "in pantry":
            formattedState = .inPantry
            
        default:
            formattedState = .unknown
        }
        
        self.init(id: id, expirationDate: expirationDate, state: formattedState, product: product)
    }
    
    init(json: JSON) {
        let jsonPantryItem = json["item"].dictionary!
        let jsonProduct = jsonPantryItem["product"]!.dictionary!
        let jsonState = jsonPantryItem["state"]!.dictionary!
        let jsoncategory = jsonProduct["category"]!.dictionary!
        
        let expirationDate = appDateFormatter.dateFromServerDate(jsonPantryItem["experationDate"]!.string!)
        
        self.init(
            id: jsonPantryItem["id"]!.string!,
            expirationDate: expirationDate,
            state: jsonState["value"]!.string!,
            product: Product(
                id: jsonProduct["id"]!.string!,
                name: jsonProduct["name"]!.string!,
                category: ProductCategory(id: jsoncategory["id"]!.string!, value: jsoncategory["value"]!.string!),
                experiaitonPeriod: jsonProduct["experiaitonPeriod"]!.string!)
        )
    }
}

struct PantryItems {
    var items: [PantryItem] = []
    
    init(json: JSON) {
        let jsonItems = json["items"].array!
        for jsonItem in jsonItems {
            items.append(PantryItem(json: jsonItem))
        }
    }
}


enum PantryItemState {
    case defualt
    case inPantry
    case unknown
}
