//
//  Store.swift
//  Pantry
//
//  Created by Carson Franklin on 4/25/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

// IMPORTANT: This is a SINGLETON.
class Store: ObservableObject {
    
    static var shared = Store()
    @Published private(set) var pantryItems: [PantryItem] = []
    @Published var pantryID = UUID().uuidString
    @Published var categories: [ProductCategory] = []
    
//    // TODO: Remove test user before releasing.
    @Published var user: User? = User(firstName: "Carson", lastName: "Franklin", username: "cfranklin", email: "franklin.carson@gmail.com", password: "password")
//    @Published var user: User?
    
    
    private var nf = NetworkFacade()
    
    init() {
        setCategories()
    }
    
    func setUser(user: User) -> Void {
        self.user = user
    }
    
    func addPantryItem(item: PantryItem) {
        pantryItems.append(item)
    }
    
    func addPantryItems(items: PantryItems) {
        pantryItems.append(contentsOf: items.items)
    }
    
    private func setCategories() {
        let service = GetProductCategoriesService()
        service.execute(onGetCategoriesCompleted)
    }
    
    private func onGetCategoriesCompleted(_ res: Result<[ProductCategory], NetworkError>) -> Void {
        DispatchQueue.main.async {
            switch res {
            case .success(let categories):
                self.categories = categories
                break
                
            case .failure(let e):
                print("There was a problem retrieving the product categories from the server:\n\(e)")
                appNotificationObservable.show(text: "Sorry there was an error loading some data. Please try again later.", mode: .error)
            }
        }
    }
}
