//
//  Settings.swift
//  Pantry
//
//  Created by Carson Franklin on 5/21/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation

public enum AppSettings {
    
    // MARK: Environment Values
    static let baseURL = getBaseURL()
    
    
    // MARK: Info.plist
    private static let infoDictionary: [String: Any] = getInfoDictionary()
    
    
    // MARK: Helper Functions
    private static func getInfoDictionary() -> [String : Any] {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError("Info.plist file could not be located while loading the environment.")
        }
        return dict
    }
    
    private static func getBaseURL() -> String {
        guard let baseURL = AppSettings.infoDictionary["BASE_URL"] as? String else {
            fatalError("The base URL found in the Info.plist is invalid.")
        }
        
        return baseURL
    }
    
    // MARK: App Settings
    static let supportedUPCs = supportedCodeTypes
    static let serverDateFormat = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
    static let appDateFormat = "MMM dd, yy"
}
