//
//  UIExtensions.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

// Extension for keyboard to dismiss
// Refrence: https://www.hackingwithswift.com/forums/swiftui/textfield-dismiss-keyboard-clear-button/240
extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
