//
//  CreatePantryItemRequest.swift
//  Pantry
//
//  Created by Carson Franklin on 7/3/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation

struct CreatePantryItemRequest: Identifiable, Codable {
    var id = UUID()
    var productID: String
    var pantryID: String
    var experationDate: Date?
}

struct CreatePantryItemRequests: Codable {
    var items: [CreatePantryItemRequest]
    
    init(items: [CreatePantryItemRequest]) {
        self.items = items
    }
    
    init(items: [PantryItem]) throws {
        self.items = []
        
        for item in items {
            guard let productID: String = item.product?.id else {
                throw ParseError.productIDNotFound
            }
            self.items.append(CreatePantryItemRequest(productID: productID, pantryID: Store.shared.pantryID))
        }
    }
    
    enum ParseError: Error {
        case productIDNotFound
    }
}
