//
//  UPCLookupRequest.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation


struct UPCLookupRequest: Codable {
    var code: String
}
