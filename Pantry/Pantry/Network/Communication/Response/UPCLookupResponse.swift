//
//  UPCLookupResponse.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation


struct UPCLookupResponse {
    var newProduct: Bool
    var name: String?
    var product: Product?
}
