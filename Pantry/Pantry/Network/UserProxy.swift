//
//  UserProxy.swift
//  Pantry
//
//  Created by Carson Franklin on 4/25/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import Alamofire

class UserProxy {
    func getUser() -> User {
        return User(firstName: "", lastName: "Doe", username: "johnDoe@example.com", email: "foo@example.com", password: "")
    }
}
