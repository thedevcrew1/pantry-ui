//
//  NetworkFacade.swift
//  Pantry
//
//  Created by Carson Franklin on 4/25/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation

// IMPORTANT: This is a SINGLETON class.
class NetworkFacade {
    
    static var shared = NetworkFacade()
    
    private var userProxy = UserProxy()

    
    func getUser() -> User {
        return userProxy.getUser()
    }
}
