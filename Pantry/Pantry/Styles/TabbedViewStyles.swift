//
//  TabbedViewStyles.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

extension UITabBarController {
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let standardAppearance = UITabBarAppearance()
        
//        standardAppearance.stackedLayoutAppearance.focused.titleTextAttributes = [.foregroundColor: UIColor.red]
//        standardAppearance.stackedLayoutAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.red]
        standardAppearance.stackedLayoutAppearance.selected.titleTextAttributes = [.foregroundColor: UIColor.primary]
        
//        standardAppearance.inlineLayoutAppearance.focused.titleTextAttributes = [.foregroundColor: UIColor.green]
//        standardAppearance.inlineLayoutAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.green]
        standardAppearance.inlineLayoutAppearance.selected.titleTextAttributes = [.foregroundColor: UIColor.primary]
        
//        standardAppearance.compactInlineLayoutAppearance.focused.titleTextAttributes = [.foregroundColor: UIColor.blue]
//        standardAppearance.compactInlineLayoutAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.blue]
        standardAppearance.compactInlineLayoutAppearance.selected.titleTextAttributes = [.foregroundColor: UIColor.primary]
        
        tabBar.standardAppearance = standardAppearance
    }
}
