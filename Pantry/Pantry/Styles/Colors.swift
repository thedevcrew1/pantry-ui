//
//  Colors.swift
//  Pantry
//
//  Created by Carson Franklin on 4/27/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {
    static let primary = Color("primary")
    static let accent = Color("accent")
    static let header = Color("header")
}

extension UIColor {
    static let primary = UIColor.init(red: 0.706, green: 0.129, blue: 0.333, alpha: 1)
}
