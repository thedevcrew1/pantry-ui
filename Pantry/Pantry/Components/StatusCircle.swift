//
//  StatusCircle.swift
//  Pantry
//
//  Created by Carson Franklin on 5/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct StatusCircle: View {
    var expired: Bool
    
    var body: some View {
        Circle()
        .foregroundColor(self.expired ? Color.accent : Color.primary)
        .opacity(0.9)
    }
}

struct StatusCircle_Previews: PreviewProvider {
    static var previews: some View {
        StatusCircle(expired: false)
    }
}
