//
//  AddPantryItemToolbar.swift
//  Pantry
//
//  Created by Carson Franklin on 6/10/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct AddPantryItemToolbar: View {
    @Binding var mode: AddItemsObservable.Mode
    var cancelBtnClicked: () -> Void
    var scanItemsBtnClicked: () -> Void
    
    var body: some View {
        
        VStack {
            
            HStack {
                if mode == .viewCategory || mode == .scanItems {
                    Button(action: {
                        withAnimation {
                            self.mode = .display
                        }
                    }) {
                        HStack {
                            Image(systemName: "chevron.left")
                                .imageScale(.medium)
                                .foregroundColor(Color.primary)
                            Text("Back")
                                .foregroundColor(Color.primary)
                        }
                    }
                } else {
                    Button(action: {
                        NavigationObserver.shared.navigateToPrevious()
                    }) {
                        HStack {
                            Image(systemName: "chevron.left")
                                .imageScale(.medium)
                                .foregroundColor(Color.accent)
                            Text("Cancel")
                                .foregroundColor(Color.accent)
                        }
                    }
                }
                
                Spacer()
                
                
                Button(action: scanItemsBtnClicked){
                    Image(systemName: "barcode.viewfinder")
                        .resizable()
                        .frame(width: 25, height: 25)
                        .foregroundColor(.primary)
                }
                
            }
            .padding(.trailing)
            .padding(.leading)
            .padding(.top)
            
            Spacer()
        }
    }
}

struct AddPantryItemToolbar_Previews: PreviewProvider {
    static var previews: some View {
        AddPantryItemToolbar(mode: Binding.constant(AddPantryItemView.Mode.display), cancelBtnClicked: {}, scanItemsBtnClicked: {})
    }
}
