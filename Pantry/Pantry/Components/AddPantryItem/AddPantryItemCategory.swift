//
//  AddPantryItemCategory.swift
//  Pantry
//
//  Created by Carson Franklin on 6/12/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct AddPantryItemCategory: View {
    let category: ProductCategory
    @ObservedObject var addItemsObservable: AddItemsObservable
    @ObservedObject var searchBarObservable: SearchBarObservable
    
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                ViewTitle(category.value, color: Color.header)
                Spacer()
            }
            .padding(.trailing)
            .padding(.leading)
            
            SearchProductsBar(searchBarObservable: searchBarObservable)
            .padding(.leading)
            .padding(.trailing)
            .padding(searchBarObservable.isEditing ? .top: .trailing)
            
            ProductSearchResults(addItemsObservable: addItemsObservable)
        }
        .onAppear {
            self.searchBarObservable.placeholder = "Search \(self.category.value)"
        }
    }
}

struct AddPantryItemCategory_Previews: PreviewProvider {
    static var previews: some View {
        AddPantryItemCategory(category: ProductCategory(id: "alsdkfj", value: "Milk"), addItemsObservable: AddItemsObservable(),searchBarObservable: SearchBarObservable())
    }
}
