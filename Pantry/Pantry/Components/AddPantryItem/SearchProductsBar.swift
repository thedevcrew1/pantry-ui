//
//  SearchProductsBar.swift
//  Pantry
//
//  Created by Carson Franklin on 6/10/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct SearchProductsBar: View {
    @ObservedObject var searchBarObservable: SearchBarObservable
    
    var body: some View {
        SearchBar(searchBarObservable: searchBarObservable, backgroundColor: Color.gray, textColor: Color.white, cancelTextColor: Color.header, borderColor: Color.gray, shadow: 3)
    }
}

struct SearchProductsBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchProductsBar(searchBarObservable: SearchBarObservable(placeHolder: "Search Products"))
    }
}
