//
//  ProductSearchResults.swift
//  Pantry
//
//  Created by Carson Franklin on 6/11/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ProductSearchResults: View {
    @ObservedObject var addItemsObservable: AddItemsObservable
    
    var body: some View {
        VStack {
            
            // Used to create a little extra padding above the list.
            HStack {
                Spacer()
                EmptyView()
            }
            .padding(.bottom)
            
            // MARK: List of search results
            ForEach((1...50).reversed(), id: \.self) { number in
                VStack {
                    Row(addItemsObservable: self.addItemsObservable)
                        .padding(.leading, 20)
                        .padding(.trailing, 20)
                        .padding(.top, 2)
                    Divider()
                        .padding(.leading)
                        .padding(.trailing)
                }
            }
        }
        .background(Color.white)
        .cornerRadius(10)
        .shadow(radius: 7)
        .padding()
    }
    
    struct Row: View {
        @ObservedObject var addItemsObservable: AddItemsObservable
        var product: Product = Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")
        
        var body: some View {
            HStack {
                Image(self.product.name.lowercased())
                    .resizable()
                    .frame(width: 30, height: 30)
                Text("Pork Chop")
                    .font(.headline)
                Spacer()
                Button(action: plusBtnTapped) {
                    Image(systemName: "plus")
                        .imageScale(.medium)
                        .foregroundColor(Color.primary)
                }
                .buttonStyle(PlainButtonStyle())
            }
            .background(Color.white)
        }
        
        
        // MARK: Row Methods
        
        func plusBtnTapped() -> Void {
            withAnimation {
                HapticEngine.shared.selectHaptic()
                let item = PantryItem(id: "\(UUID())", expirationDate: Date(), state: PantryItemState.defualt, product: product)
                self.addItemsObservable.add(item: item)
            }
        }
    }
}

struct ProductSearchResults_Previews: PreviewProvider {
    static var previews: some View {
        ProductSearchResults(addItemsObservable: AddItemsObservable())
    }
}
