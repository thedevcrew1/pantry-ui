//
//  AddPantryItemDisplayView.swift
//  Pantry
//
//  Created by Carson Franklin on 6/10/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct AddPantryItemDisplayView: View {
    @State var twoDCategoryList: [[ProductCategory]]?
    @ObservedObject var searchBarObservable: SearchBarObservable
    var categoryTileClicked: (_ category: ProductCategory) -> Void
    
    init(searchBarObservable: SearchBarObservable, categoryTileClicked: @escaping (_ category: ProductCategory) -> Void) {
        self.searchBarObservable = searchBarObservable
        self.categoryTileClicked = categoryTileClicked
    }
    
    var body: some View {
        VStack {
            // MARK: Title
            if !searchBarObservable.isEditing {
                HStack {
                    Spacer()
                    ViewTitle("Add Items", color: Color.header)
                    Spacer()
                }
                .padding(.top)
                .padding(.trailing)
                .padding(.leading)
                .transition(.move(edge: .top))
            }
            
            
            // MARK: Search Products Bar
            HStack {
                SearchProductsBar(searchBarObservable: searchBarObservable)
                    .padding(.leading)
                    .padding(.trailing)
                    .padding(searchBarObservable.isEditing ? .top: .trailing)
                    .background(Color.clear)
            }
            
            // MARK: Categories Tiles
            if twoDCategoryList != nil && !searchBarObservable.isEditing {
                VStack {
                    ForEach(self.twoDCategoryList!, id: \.self) { listOfCategories in
                        HStack {
                            ForEach(listOfCategories) { category in
                                Button(action: { self.categoryTileClicked(category) }) {
                                    CategoryTile(category: category, size: 100, imageSize: 50)
                                        .padding(.leading, 20)
                                        .padding(.trailing, 20)
                                        .padding(.bottom)
                                }
                                .buttonStyle(PlainButtonStyle())
                            }
                        }
                    }
                }
                .padding(.top)


            }
            
            if !searchBarObservable.isEditing {
                Spacer()
            }
        }
        .onAppear {
            self.setTwoDCategoryList()
        }
    }
    
    
    // MARK: Methods
    
    func setTwoDCategoryList() -> Void {
        let categories = Store.shared.categories
        let numPerRow = 2
        var twoDCategoryList: [[ProductCategory]] = []
        
        if categories.count < 1 {
            self.twoDCategoryList = twoDCategoryList
            return
        }
        
        var i = 0
        for j in 0...categories.count - 1 {
            if j != 0 && j % numPerRow == 0 {
                i += 1
            }
            
            if i > twoDCategoryList.count - 1 {
                twoDCategoryList.append([categories[j]])
            } else {
                twoDCategoryList[i].append(categories[j])
            }
        }
        
        
        self.twoDCategoryList = twoDCategoryList
    }
}

struct AddPantryItemDisplayView_Previews: PreviewProvider {
    static var previews: some View {
        AddPantryItemDisplayView(searchBarObservable: SearchBarObservable(), categoryTileClicked: {_ in })
    }
}
