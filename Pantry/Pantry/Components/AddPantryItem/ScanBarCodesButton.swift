//
//  ScanBarCodesButton.swift
//  Pantry
//
//  Created by Carson Franklin on 6/11/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ScanBarCodesButton: View {
    var action: () -> Void
    var body: some View {
        Button(action: action) {
            HStack {
                Text("Scan Barcodes")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding(.trailing)
                Image(systemName: "barcode.viewfinder")
                .resizable()
                .frame(width: 25, height: 25)
                .foregroundColor(.white)
            }
        }
        .padding()
        .background(Color.primary)
        .cornerRadius(10)
        .shadow(radius: 3, y: 2)
    }
}

struct ScanBarCodesButton_Previews: PreviewProvider {
    static var previews: some View {
        ScanBarCodesButton(action: {})
    }
}
