//
//  ItemCountBadge.swift
//  Pantry
//
//  Created by Carson Franklin on 6/18/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ItemCountBadge: View {
    @ObservedObject var addItemsObservable: AddItemsObservable
    
    var body: some View {
        let number = Text("\(addItemsObservable.items.count)")
            .fontWeight(.bold)
            .font(Font.system(size: 14, design: .default))
            .foregroundColor(Color.white)
            .padding(.leading, 10)
            .padding(.trailing, 10)
            .padding(.top, 6)
            .padding(.bottom, 6)
            .background(Color.accent)
            .cornerRadius(20)
        
        return number
    }
}

struct ItemCountBadge_Previews: PreviewProvider {
    static var previews: some View {
        ItemCountBadge(addItemsObservable: AddItemsObservable())
    }
}
