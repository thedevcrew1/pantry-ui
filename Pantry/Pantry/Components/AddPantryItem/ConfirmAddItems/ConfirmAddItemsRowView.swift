//
//  ConfirmAddItemsRowView.swift
//  Pantry
//
//  Created by Carson Franklin on 6/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ConfirmAddItemsRowView: View {
    let item: PantryItem
    @ObservedObject var addItemsObservable: AddItemsObservable
    @State var date = Date()
    @State var mode: Mode = .display
    
    var formattedDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    var body: some View {
        VStack {
            HStack {
                Image(self.item.product?.name.lowercased() ?? "Meat")
                    .resizable()
                    .frame(width: 30, height: 30)
                Text("Pork Chop")
                    .font(.headline)
                Spacer()
                Text(formattedDate)
                    .foregroundColor(Color.accent)
                Image(systemName: "trash")
                    .foregroundColor(Color.header)
                    .padding(.leading)
                    .onTapGesture {
                        self.deleteBtnTapped()
                }
            }
            .background(Color.white)
            .onTapGesture {
                self.rowTapped()
            }
            
            if mode == .edit {
                DatePicker(selection: $date, in: Date()..., displayedComponents: [DatePickerComponents.date]) {
                    EmptyView()
                }
                .frame(maxWidth: 200)
                .padding()
                .transition(.scale)
            }
        }
    }
    
    // MARK: Methods
    
    func rowTapped() {
        withAnimation {
            switch self.mode {
            case .display:
                self.mode = .edit
                break
                
            case .edit:
                self.mode = .display
                break
            }
        }
    }
    
    func deleteBtnTapped() {
        withAnimation {
            self.addItemsObservable.remove(item: item)
        }
    }
    
    enum Mode {
        case display
        case edit
    }
}

struct ConfirmAddItemsRowView_Previews: PreviewProvider {
    static var previews: some View {
        ConfirmAddItemsRowView(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 weeks")), addItemsObservable: AddItemsObservable())
    }
}
