//
//  ConfirmAddItemsView.swift
//  Pantry
//
//  Created by Carson Franklin on 6/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ConfirmAddItemsView: View {
    @ObservedObject var addItemsObservable: AddItemsObservable
    let saveBtnClicked: () -> Void
    
    var body: some View {
        ZStack {
            
            BackgroundImage(mode: .inverted)
            
            ScrollView {
                VStack {
                    HStack {
                        ViewTitle("Confirm Items", color: Color.header)
                        .padding(.leading)
                        .padding(.top)
                        Spacer()
                    }
                    
                    if addItemsObservable.items.count > 0 {
                        ItemsToConfirmView(addItemsObservable: addItemsObservable)
                        .padding()
                            .padding(.bottom, 40)
                    }
                }
            }
            
            if addItemsObservable.items.count > 0 {
                
                VStack {
                    Spacer()
                    HStack {
                        Spacer()
                        SaveItemsBtn(action: localSaveBtnClicked)
                            .padding(.trailing, 20)
                            .padding(.bottom, 30)
                            .padding(.top, 20)
                        Spacer()
                    }
                    .background(LinearGradient(gradient: Gradient(colors: [.accent, Color.clear]), startPoint: .bottom, endPoint: .top))
                }
                .edgesIgnoringSafeArea(.bottom)
            } else {
                Text("Please select some items to add")
                    .font(.headline)
                    .foregroundColor(Color.header)
                    .transition(.scale)
            }
        }
    }
    
    func localSaveBtnClicked() -> Void {
        saveBtnClicked()
        let service = CreatePantryItemsService()
        do {
            try service.execute(items: CreatePantryItemRequests(items: addItemsObservable.items), onCompleteHandler: onLocalSaveComplete)
        } catch(let e) {
            print("An exception occurred while attempting to save the pantry items:\n \(e.localizedDescription)")
        }
    }
    
    func onLocalSaveComplete(_ res: Result<PantryItems, CreatePantryItemsService.CreatePantryItemError>) -> Void {
        DispatchQueue.main.async {
            switch res {
            case .success(let items):
                Store.shared.addPantryItems(items: items)
                appNotificationObservable.show(text: "\(items.items.count) items were successfully added to your pantry!", mode: .success)
            case .failure(let e):
                print("Creating items failed \(e)")
                appNotificationObservable.show(text: "Sorry something went wrong adding your items to your pantry. Please try again later.", mode: .error)
            }
        }
    }
}

struct ConfirmAddItemsView_Previews: PreviewProvider {
    static var previews: some View {
        let addItemsObservable = AddItemsObservable()
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        addItemsObservable.add(item: PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks")))
        return ConfirmAddItemsView(addItemsObservable: addItemsObservable){}
    }
}
