//
//  SaveItemsBtn.swift
//  Pantry
//
//  Created by Carson Franklin on 6/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct SaveItemsBtn: View {
    let action: () -> Void
    
    var body: some View {
        Button(action: action) {
            Text("Save Items")
                .foregroundColor(Color.white)
                .padding()
        }
        .buttonStyle(PlainButtonStyle())
        .background(Color.primary)
        .cornerRadius(10)
        .shadow(radius: 3, y: 2)
    }
}

struct SaveItemsBtn_Previews: PreviewProvider {
    static var previews: some View {
        SaveItemsBtn(action: {})
    }
}
