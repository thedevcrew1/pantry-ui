//
//  ItemsToConfirmView.swift
//  Pantry
//
//  Created by Carson Franklin on 6/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ItemsToConfirmView: View {
    @ObservedObject var addItemsObservable: AddItemsObservable
    
    var body: some View {
        VStack {
            
            // Used to create a little extra padding above the list.
            HStack {
                Spacer()
                EmptyView()
            }
            .padding(.bottom)
            
            // MARK: List of search results
            ForEach(addItemsObservable.items) { item in
                VStack {
                    ConfirmAddItemsRowView(item: item, addItemsObservable: self.addItemsObservable)
                        .padding(.leading, 20)
                        .padding(.trailing, 20)
                        .padding(.top, 2)
                    Divider()
                        .padding(.leading)
                        .padding(.trailing)
                }
            }
        }
        .background(Color.white)
        .cornerRadius(10)
        .shadow(radius: 7)
    }
}

struct ItemsToConfirmView_Previews: PreviewProvider {
    static var previews: some View {
        ItemsToConfirmView(addItemsObservable: AddItemsObservable())
    }
}
