//
//  AddItemsButton.swift
//  Pantry
//
//  Created by Carson Franklin on 6/17/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct AddItemsButton: View {
    @ObservedObject var addItemsObservable: AddItemsObservable
    var action: () -> Void
    
    var body: some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                
                Button(action: action) {
                    HStack {
                        ZStack {
                            HStack {
                                Image(systemName: "plus")
                                .foregroundColor(Color.white)
                                Text("Add")
                                    .font(.headline)
                                    .foregroundColor(Color.white)
                                    
                                
                            }
                            .padding(10)
                            .padding(.leading)
                            .padding(.trailing)
                            .background(Color.primary)
                            .cornerRadius(15)
                            .shadow(radius: 7, x: 2, y: 3)
                            ItemCountBadge(addItemsObservable: addItemsObservable)
                                .shadow(radius: 7, x: 2, y: 3)
                                .offset(x:40, y: -25)
                        }
                    }
                    .padding(.trailing, 20)
                    .padding(.bottom, 30)
                }
            }
        }
    }
}

struct AddItemsButton_Previews: PreviewProvider {
    static var previews: some View {
        AddItemsButton(addItemsObservable: AddItemsObservable(), action: {})
    }
}
