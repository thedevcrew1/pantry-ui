//
//  AddPantryItemBackground.swift
//  Pantry
//
//  Created by Carson Franklin on 6/11/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct AddPantryItemBackground: View {
    var body: some View {
        VStack {
            Spacer()
            HStack {
                GeometryReader { geoProxy in
                    Circle()
                        .foregroundColor(Color.accent)
                        .frame(width: geoProxy.size.width * 2)
                        .offset(x:geoProxy.size.width * -0.45 ,y: geoProxy.size.width * 1)
                        .animation(.spring())
                        
                }
            }
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct AddPantryItemBackground_Previews: PreviewProvider {
    static var previews: some View {
        AddPantryItemBackground()
    }
}
