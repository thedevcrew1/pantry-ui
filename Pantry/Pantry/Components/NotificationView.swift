//
//  NotificationView.swift
//  Pantry
//
//  Created by Carson Franklin on 7/13/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    
    
    @ObservedObject var notificationObservable: NotificationObservable
    @State var dragOffset: CGFloat = CGFloat.zero
    
    
    var body: some View {
        VStack {
            if notificationObservable.mode != .hidden {
                VStack(alignment:.leading) {
                    
                    if notificationObservable.mode == .success {
                        NotificationTitle(text: notificationObservable.title == "" ? "Success" : notificationObservable.title, color: Color.green)
                    } else if notificationObservable.mode == .error {
                        NotificationTitle(text: notificationObservable.title == "" ? "Error" : notificationObservable.title, color: Color.red)
                    } else if notificationObservable.mode == .info {
                        NotificationTitle(text: notificationObservable.title, color: Color.blue)
                    }
                    
                    Text(notificationObservable.text)
                        .font(.callout)
                        .padding(.top, 5)
                        .padding(.leading)
                        .padding(.trailing)
                        .padding(.bottom)
                }
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 7)
                .animation(.spring())
                .offset(y: self.dragOffset)
                .padding(.leading)
                .padding(.trailing)
                .transition(.move(edge: .top))
                .gesture(DragGesture()
                .onChanged { val in
                    if val.translation.height > 50 {
                        self.dragOffset = 50 // Prevents dragging down past 50 units.
                    } else {
                        self.dragOffset = val.translation.height
                    }
                    self.notificationObservable.preventDissmiss = true
                }
                .onEnded { val in
                    self.dragOffset = CGFloat.zero
                    
                    if val.translation.height < -40 {
                        self.notificationObservable.mode = .hidden
                    }
                    
                    self.notificationObservable.preventDissmiss = false
                })
            }
            Spacer()
        }
        .background(Color.clear)
    }
    
    
    struct NotificationTitle: View {
        
        let text: String
        let color: Color
        
        var body: some View {
            Text(text)
                .font(.footnote)
                .foregroundColor(color)
                .padding(.leading)
                .padding(.top, 5)
        }
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView(notificationObservable: NotificationObservable())
    }
}
