//
//  CategoryTile.swift
//  Pantry
//
//  Created by Carson Franklin on 6/10/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct CategoryTile: View {
    let category: ProductCategory
    
    var size: CGFloat = 100
    @State var imageSize: CGFloat = 55
    
    var body: some View {
        
        VStack {
            Image(UIImage(named: category.value) != nil ? category.value : "supermarket")
                .resizable()
                .frame(width:self.imageSize, height: self.imageSize)
                .background(Color.white)
            Text(category.value)
                .font(.caption)
        }
        .padding()
        .frame(width: size, height: size)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(radius: 4, x: 2, y: 2)
    }
}

struct CategoryTile_Previews: PreviewProvider {
    static var previews: some View {
        CategoryTile(category: ProductCategory(id: "asdflkj", value: "Milk"))
    }
}
