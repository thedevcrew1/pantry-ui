//
//  QuantityCircle.swift
//  Pantry
//
//  Created by Carson Franklin on 5/12/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct QuantityCircle: View {
    var quantity: Int
    var expired: Bool
    
    var body: some View {
        let number = Text("\(quantity)")
            .fontWeight(.bold)
            .font(Font.system(size: 14, design: .default))
            .foregroundColor(Color.white)
        
        return StatusCircle(expired: expired)
            .frame(width:21)
            .overlay(number)
    }
}

struct QuantityCircle_Previews: PreviewProvider {
    static var previews: some View {
        QuantityCircle(quantity: 3, expired: false)
    }
}
