//
//  ScanItemsViewInstructionPrompt.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ScanItemsViewInstructionPrompt: View {
    var body: some View {
        HStack {
            Text("Scan an Item")
                .font(.title)
                .foregroundColor(Color.white)
            Image(systemName: "barcode.viewfinder")
                .imageScale(.large)
                .foregroundColor(Color.white)
            Spacer()
        }
        .padding(.bottom, 80)
        .padding(.leading, 30)
    }
}

struct ScanItemsViewInstructionPrompt_Previews: PreviewProvider {
    static var previews: some View {
        ScanItemsViewInstructionPrompt()
    }
}
