//
//  AdjustScannedItemComponent.swift
//  Pantry
//
//  Created by Carson Franklin on 5/26/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct AdjustScannedItemComponent: View {
    var body: some View {
        VStack(alignment: .center) {
            HStack {
                Button(action: {}) {
                    Circle()
                        .frame(width: 21, height: 21)
                        .overlay(Image(systemName: "chevron.left")
                            .foregroundColor(Color.white)
                            .imageScale(.small))
                }
                .buttonStyle(PlainButtonStyle())
                .shadow(radius: 3)
                
                Text("May 19, 2020")
                    .padding()
                
                Button(action: {}) {
                    Circle()
                        .frame(width: 21, height: 21)
                        .overlay(Image(systemName: "chevron.right")
                            .foregroundColor(Color.white)
                            .imageScale(.small))
                }
                .buttonStyle(PlainButtonStyle())
                .shadow(radius: 3)
            }
            
            
            
            Button(action: {}) {
                Text("Save")
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                .padding(3)
                .frame(width: 100)
                .background(Color.black)
                .cornerRadius(10)
                .shadow(radius: 3)
            }
        }
        .padding()
        .frame(width: 270)
        .background(Color.white)
        .cornerRadius(10)
        .shadow(radius: 7)
    }
}

struct AdjustScannedItemComponent_Previews: PreviewProvider {
    static var previews: some View {
        AdjustScannedItemComponent()
    }
}
