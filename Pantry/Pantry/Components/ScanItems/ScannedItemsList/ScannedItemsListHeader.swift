//
//  ScannedItemsListHeader.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ScannedItemsListHeader: View {
    @ObservedObject var scanItemsObservable: ScanItemsObservable
    
    var body: some View {
        HStack {
            ViewTitle("Scanned Items", color: Color.header)
                .padding(.leading, 5)
                .transition(.move(edge: .top))
            Spacer()
            if scanItemsObservable.mode == .loading {
                LoadingSpinner()
                .frame(width: 40, height: 40)
                .foregroundColor(Color.accent)
                .padding(.trailing)
                .transition(.scale)
            }
        }
    }
}

struct ScannedItemsListHeader_Previews: PreviewProvider {
    static var previews: some View {
        ScannedItemsListHeader(scanItemsObservable: ScanItemsObservable(addItemsObservable: AddItemsObservable()))
    }
}
