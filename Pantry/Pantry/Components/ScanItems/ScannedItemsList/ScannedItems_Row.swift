//
//  ScannedItems_Row.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ScannedItems_Row: View {
    let item: PantryItem
    var name: String {
        return item.product?.name ?? "N/A"
    }
    
    var deleteBtnClicked: (_ item: PantryItem) -> Void
    var itemTapped: (_ item: PantryItem) -> Void
    
    var body: some View {
        HStack {
            VStack(alignment: .leading){
                Text(name)
                    .font(.headline)
                Text("Expires \(item.expirationDate)")
                    .font(.caption)
                    .foregroundColor(Color.gray)
            }
            .onTapGesture {
                self.itemTapped(self.item)
            }
            Spacer()
            
            Image(systemName: "trash")
                .imageScale(.large)
                .foregroundColor(Color.gray)
                .onTapGesture {
                    self.deleteBtnClicked(self.item)
            }
            
        }
        .listRowBackground(Color.white)
        .frame(maxWidth: .infinity)
    }
}


struct ScannedItems_Row_Previews: PreviewProvider {
    static var previews: some View {
        ScannedItems_Row(item: PantryItem(id: ";lkj;lkasdf", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "gnioefnladskjf", name: "Milk", category: ProductCategory(id: "weoingnalie22fasd;lkj", value: "Dairy"), experiaitonPeriod: "2 weeks")), deleteBtnClicked: {_ in}, itemTapped: {_ in})
    }
}
