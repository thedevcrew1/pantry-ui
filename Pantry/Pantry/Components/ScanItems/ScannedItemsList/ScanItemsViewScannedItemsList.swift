//
//  ScanItemsViewScannedItemsList.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ScanItemsViewScannedItemsList: View {
    @ObservedObject var scanItemsObservable: ScanItemsObservable
    
    var body: some View {
        ScrollView {
            ScannedItemsListHeader(scanItemsObservable: scanItemsObservable)
            
            // MARK: List of Items
            VStack {
                ForEach(scanItemsObservable.scannedItems, id: \.id) { item in
                    VStack {
                        ScannedItems_Row(item: item, deleteBtnClicked: self.scanItemsObservable.delete, itemTapped: {_ in })
                            .padding(.top, 6)
                            .padding(.bottom, 6)
                    }
                }
            }
            .padding()
            .background(Color.white)
            .cornerRadius(10)
            .shadow(radius: 4)
            .padding(5)
        }
        
    }
}


struct ScanItemsViewScannedItemsList_Previews: PreviewProvider {
    static var previews: some View {
        ScanItemsViewScannedItemsList(scanItemsObservable: ScanItemsObservable(addItemsObservable: AddItemsObservable(), newProductSheetDisplayed: {}))
    }
}
