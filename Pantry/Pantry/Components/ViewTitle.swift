//
//  ViewTitle.swift
//  Pantry
//
//  Created by Carson Franklin on 5/4/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ViewTitle: View {
    var text: String
    var color: Color = Color.white
    
    init(_ text: String) {
        self.text = text
    }
    
    init(text: String) {
        self.text = text
    }
    
    init(_ text: String, color: Color) {
        self.text = text
        self.color = color
    }
    
    init(text: String, color: Color) {
        self.text = text
        self.color = color
    }
    
    var body: some View {
        Text(text)
        .foregroundColor(color)
        .font(.largeTitle)
        .fontWeight(.bold)
    }
}

struct ViewTitle_Previews: PreviewProvider {
    static var previews: some View {
        ViewTitle("View Title")
    }
}
