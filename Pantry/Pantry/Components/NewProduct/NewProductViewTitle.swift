//
//  NewProductViewTitle.swift
//  Pantry
//
//  Created by Carson Franklin on 7/15/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct NewProductViewTitle: View {
    var body: some View {
        HStack {
            ViewTitle("New Product", color: Color.header)
            Spacer()
        }
    }
}

struct NewProductViewTitle_Previews: PreviewProvider {
    static var previews: some View {
        NewProductViewTitle()
    }
}
