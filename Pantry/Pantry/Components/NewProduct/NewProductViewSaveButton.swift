//
//  NewProductViewSaveButton.swift
//  Pantry
//
//  Created by Carson Franklin on 7/15/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct NewProductViewSaveButton: View {
    let action: () -> Void
    
    var body: some View {
        HStack {
            Spacer()
            Button(action: action) {
                Text("Save")
                    .foregroundColor(Color.white)
                    .padding()
                    .padding(.leading)
                    .padding(.trailing)
            }
            .buttonStyle(PlainButtonStyle())
            .background(Color.primary)
            .cornerRadius(10)
            .shadow(radius: 7)
            .padding()
            Spacer()
        }
    }
}

struct NewProductViewSaveButton_Previews: PreviewProvider {
    static var previews: some View {
        NewProductViewSaveButton(action: {})
    }
}
