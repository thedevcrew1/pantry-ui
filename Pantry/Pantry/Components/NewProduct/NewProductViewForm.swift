//
//  NewProductViewForm.swift
//  Pantry
//
//  Created by Carson Franklin on 7/15/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct NewProductViewForm: View {
    
    @ObservedObject var newProductObservable: NewProductObservable
    
    var body: some View {
        Form {
            Section(header: Text("Name")) {
                TextField("ex: Bread", text: $newProductObservable.product.name)
            }
            
            Section {
                Picker(selection: $newProductObservable.selectedProductIndex, label: Text("Category")) {
                    ForEach(0 ..< Store.shared.categories.count) {
                        Text(Store.shared.categories[$0].value)
                    }
                }
            }

            Section(header: Text("Experiation Period")) {
                Picker(selection: $newProductObservable.intervalUnitIndex, label: Text("Period")) {
                    ForEach(0 ..< IntervalUnits.allCases.count){
                        Text(IntervalUnits.allCases[$0].rawValue)
                    }
                }
                Stepper("\(newProductObservable.intervalQuantity) \(IntervalUnits.allCases[newProductObservable.intervalUnitIndex].rawValue)", value: $newProductObservable.intervalQuantity)
            }
        }
    }
}

struct NewProductViewForm_Previews: PreviewProvider {
    static var previews: some View {
        NewProductViewForm(newProductObservable: NewProductObservable(product: Product(id: UUID().uuidString, name: "", category: ProductCategory(id: UUID().uuidString, value: "NA"), experiaitonPeriod: "")))
    }
}
