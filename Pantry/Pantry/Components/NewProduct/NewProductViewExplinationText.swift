//
//  NewProductViewExplinationText.swift
//  Pantry
//
//  Created by Carson Franklin on 7/15/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct NewProductViewExplinationText: View {
    var body: some View {
        Text("We've never seen this product before. Please enter the information below and we'll remember it!")
            .font(.callout)
            .padding(12)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.gray, lineWidth: 2)
            )
    }
}

struct NewProductViewExplinationText_Previews: PreviewProvider {
    static var previews: some View {
        NewProductViewExplinationText()
    }
}
