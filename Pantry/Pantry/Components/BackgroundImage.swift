//
//  BackgroundImage.swift
//  Pantry
//
//  Created by Carson Franklin on 5/27/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct BackgroundImage: View {
    var mode: Mode = .mainSubject
    
    var body: some View {
        GeometryReader { geoProxy in
            Circle()
            .foregroundColor(Color.accent)
            .frame(width: geoProxy.size.width * 2)
                .offset(y: self.calcYOffset(geoProxy: geoProxy))
        }
    }
    
    // MARK: Methods
    
    func calcYOffset(geoProxy: GeometryProxy) -> CGFloat {
        switch mode {
        case .mainSubject:
            return -geoProxy.size.width * 1.2
            
        case .details:
            return -geoProxy.size.width * 1.3
        
        case .inverted:
            return geoProxy.size.width * 1.2
        }
    }
    
    enum Mode {
        case mainSubject
        case details
        case inverted
    }
}

struct BackgroundImage_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundImage()
    }
}
