//
//  SearchBar.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI
import UIKit
import Foundation


class SearchBarObservable: ObservableObject {
    @Published var text: String = ""
    @Published var isEditing = false {
        willSet {
            objectWillChange.send()
            if isEditingUpdated != nil {
                DispatchQueue.main.async {
                    self.isEditingUpdated!(newValue)
                }
            }
            
        }
    }
    
    @Published var placeholder: String?
    var isEditingUpdated: ((_ newIsEditingVal: Bool) -> Void)?
    
    init() {
    }
    
    init(placeHolder: String) {
        self.placeholder = placeHolder
    }
    
    init(isEditingUpdated: @escaping (_ newIsEditingVal: Bool) -> Void) {
        self.isEditingUpdated = isEditingUpdated
    }
    
    init(placeHolder: String, isEditingUpdated: @escaping (_ newIsEditingVal: Bool) -> Void) {
        self.placeholder = placeHolder
        self.isEditingUpdated = isEditingUpdated
    }
    
    func updatePlaceHolder(_ placeholder: String) {
        self.placeholder = placeholder
    }
    
    func registerIsEditingUpdatedHandler(_ handler: @escaping (_ newIsEditingVal: Bool) -> Void) {
        self.isEditingUpdated = handler
    }
    
}

struct SearchBar: View {
    @ObservedObject var searchBarObservable: SearchBarObservable
    var backgroundColor: Color = Color.accent
    var textColor: Color = Color.white
    var cancelTextColor: Color = Color.white
    var borderColor: Color = Color.white
    var shadow: CGFloat = 0
    
    var body: some View {
        HStack {
            
            ZStack(alignment: .leading) {
                if searchBarObservable.text.isEmpty { Text(searchBarObservable.placeholder ?? "Search").foregroundColor(textColor) }
                TextField("", text: $searchBarObservable.text)
            }
            .foregroundColor(textColor)
            .padding(7)
            .padding(.horizontal, 25)
            .background(backgroundColor)
            .opacity(0.87)
            .cornerRadius(16)
            .shadow(radius: shadow, y: shadow)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(borderColor, lineWidth: 1)
            )
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(textColor)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)
                        
                        if searchBarObservable.isEditing {
                            Button(action: {
                                self.searchBarObservable.text = ""
                            }) {
                                Image(systemName: "multiply.circle.fill")
                                    .foregroundColor(textColor)
                                    .padding(.trailing, 8)
                                    .shadow(radius: shadow / 2, y: shadow / 2)
                            }
                        }
                    }
            )
                .padding(.horizontal, 10)
                .onTapGesture {
                    self.searchBarObservable.isEditing = true
            }
            .animation(.spring())
            
            if searchBarObservable.isEditing {
                Button(action: {
                    withAnimation {
                        self.searchBarObservable.isEditing = false
                        self.searchBarObservable.text = ""
                        
                        // Dismiss the keyboard
                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    }
                }) {
                    Text("Cancel")
                        .foregroundColor(cancelTextColor)
                }
                .buttonStyle(PlainButtonStyle())
                .padding(.trailing, 10)
                .transition(.move(edge: .trailing))
                .animation(.default)
            }
        }
    }
}



struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBar(searchBarObservable: SearchBarObservable())
    }
}
