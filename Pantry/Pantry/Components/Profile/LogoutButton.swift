//
//  LogoutButton.swift
//  Pantry
//
//  Created by Carson Franklin on 4/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct LogoutButton: View {
    var action: () -> Void
    
    var body: some View {
        Button(action: { self.action() }) {
            Text("Logout")
                .foregroundColor(Color.white)
            .frame(maxWidth: 300)
        }
        .padding()
        .background(Color.accent)
        .cornerRadius(10)
    }
}

struct LogoutButton_Previews: PreviewProvider {
    static var previews: some View {
        LogoutButton(action: {})
    }
}
