//
//  PantryItemDetails.swift
//  Pantry
//
//  Created by Carson Franklin on 5/7/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct PantryItemDetails: View {
    
    var item: PantryItem
    var productName: String
    var expired: Bool
    
    var body: some View {
        VStack {
            
            VStack(alignment: .leading) {
                HStack(alignment: .center) {
                    StatusCircle(expired: expired)
                        .frame(width: 15)
                    Text(productName)
                    .font(.headline)
                    Spacer()
                }
                .padding(.bottom)
                
                // bought on
                HStack {
                    Text("Bought")
                        .font(.footnote)
                        .foregroundColor(Color.gray)
                    Spacer()
                    Text("May 12, 2020")
                        .font(.footnote)
                }
                .padding(.bottom)
                // expiring in
                HStack {
                    Text("Expires")
                        .font(.footnote)
                        .foregroundColor(Color.gray)
                    Spacer()
                    Text("7 Days")
                        .font(.footnote)
                        .fontWeight(.bold)
                        .foregroundColor(Color.primary)
                }.padding(.top, 5)
                
                // Used Up button
                HStack {
                    
                    Spacer()
                    Button(action: {}) {
                        Image(systemName: "trash")
                            .imageScale(.medium)
                            .foregroundColor(Color.gray)
                            .background(Color.white)
                    }
                }
                .padding(.top, 10)
            }
            Spacer()
        }
        .padding()
        .background(Color.white)
        .cornerRadius(10)
        .shadow(radius: 5)
    }
}


struct PantryItemDetailsItem_Previews: PreviewProvider {
    static var previews: some View {
        PantryItemDetails(item: PantryItem(id: "foo", expirationDate: Date(), state: PantryItemState.defualt), productName: "Pork Chop", expired: false)
    }
}
