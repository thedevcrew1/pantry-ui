//
//  PantryHeader.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct PantryHeader: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    var profileBtnClicked: () -> Void
    var expiredItemsBtnClicked: () -> Void
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    ViewTitle(text: "Pantry")
                    Button(action: self.expiredItemsBtnClicked){
                        Text("You have")
                            .font(.subheadline)
                            .foregroundColor(Color.white) +
                            
                            Text(" 10 items ")
                                .fontWeight(.bold)
                                .font(.subheadline)
                                .foregroundColor(Color.white) +
                            
                            Text("about to expire")
                                .font(.subheadline)
                                .foregroundColor(Color.white)
                    }
                }
                
                Spacer()
                Button(action: { self.profileBtnClicked() }){
                    Image(systemName: "person.circle.fill")
                        .resizable()
                        .foregroundColor(Color.white)
                        .frame(width: 32, height: 32)
                }
            }
            .padding(.bottom, 8)
        }
    }
    
    enum Mode {
        case display
        case edit
        case titleAndProfileHidden
    }
}


struct PantryHeader_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PantryHeader(profileBtnClicked: {}, expiredItemsBtnClicked: {})
            
            PantryHeader(profileBtnClicked: {}, expiredItemsBtnClicked: {})
                .environment(\.colorScheme, .dark)
        }
        .previewLayout(.fixed(width: 400, height: 200))
    }
}
