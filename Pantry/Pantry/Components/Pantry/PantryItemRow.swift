//
//  PantryItemRow.swift
//  Pantry
//
//  Created by Carson Franklin on 5/20/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct PantryItemRow: View {
    
    let item: PantryItem = PantryItem(id: "398401", expirationDate: Date(), state: PantryItemState.defualt, product: Product(id: "aldsfwe39", name: "Meat", category: ProductCategory(id: "4rweoit394", value: "Meat"), experiaitonPeriod: "2 Weeks"))
    
    var body: some View {
        
        CustomNavigationLink(destination: ProductDetails(product: self.item.product!), background: .details, backButtonColor: .white) {
            HStack {
                Image(self.item.product?.name.lowercased() ?? "Meat")
                    .resizable()
                    .frame(width: 30, height: 30)
                Text("Pork Chop")
                    .font(.headline)
                Spacer()
                Text("7 Days")
                    .font(.subheadline)
                    .fontWeight(.bold)
                    .foregroundColor(self.item.state == .defualt ? Color.primary : Color.secondary)
            }
            .background(Color.white)
        }
    }
}
    
    struct PantryItemRow_Previews: PreviewProvider {
        static var previews: some View {
            PantryItemRow()
        }
}
