//
//  PantryProductRow.swift
//  Pantry
//
//  Created by Carson Franklin on 5/1/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI
import CoreHaptics

struct PantryProductRow: View {
    var product: Product = Product(id: "foo123", name: "Pork Chop", category: ProductCategory(id: "foo123", value: "Meat"), experiaitonPeriod: "1 week", items: [PantryItem(id: "foo1", expirationDate: Date(), state: PantryItemState.defualt), PantryItem(id: "foo2", expirationDate: Date(), state: PantryItemState.defualt), PantryItem(id: "foo3", expirationDate: Date(), state: PantryItemState.defualt), PantryItem(id: "foo4", expirationDate: Date(), state: PantryItemState.defualt)])
    
    @ObservedObject var selectedProducts = SelectedProductsObservable()
    
    func deselect() {
        HapticEngine.shared.selectHaptic()
        self.selectedProducts.remove(self.product)
    }
    
    func select() {
        HapticEngine.shared.selectHaptic()
        self.selectedProducts.append(self.product)
    }
    
    var body: some View {
        CustomNavigationLink(destination: ProductDetails(product: product), background: BackgroundImage.Mode.details, backButtonColor: Color.white) {
            HStack {
                // Show if the item is not in the list of selected products
                if !self.selectedProducts.products.contains(where: {selectedProduct in
                    return selectedProduct.id == self.product.id
                }) {
                    Button(action: self.select) {
                        Image(systemName: "circle")
                            .imageScale(.large)
                            .foregroundColor(Color.primary)
                    }
                }
                
                // Show if the item is in the list of selected products
                if self.selectedProducts.products.contains(where: {selectedProduct in
                    return selectedProduct.id == self.product.id
                }) {
                    Button(action: self.deselect) {
                        Image(systemName: "circle.fill")
                            .imageScale(.large)
                            .foregroundColor(Color.primary)
                    }
                }
                Text("Pork Chop")
                Spacer()
                QuantityCircle(quantity: 2, expired: false)
                QuantityCircle(quantity: 1, expired: true)
                
            }
        }
    }
}

struct PantryProductRow_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
