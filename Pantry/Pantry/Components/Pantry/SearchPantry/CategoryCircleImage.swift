//
//  CategoryCircleImage.swift
//  Pantry
//
//  Created by Carson Franklin on 5/1/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct CategoryCircleImage: View {
    let category: ProductCategory
    var size: CGFloat = 100
    @State var imageSize: CGFloat = 55
    
    var body: some View {
        VStack {
            Image(UIImage(named: category.value) != nil ? category.value : "supermarket")
            .resizable()
                .frame(width:self.imageSize, height: self.imageSize)
                .background(Color.white)
            Text(category.value)
                .font(.caption)
        }
        .padding()
        .frame(width: size, height: size)
        .background(Color.white)
        .clipShape(Circle())
        .shadow(radius: 8, x: 2, y: 10)
    }
}

struct CategoryCircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CategoryCircleImage(category: ProductCategory(id: "asdf", value: "Milk"))
    }
}
