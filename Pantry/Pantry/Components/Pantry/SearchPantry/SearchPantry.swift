//
//  SearchPantry.swift
//  Pantry
//
//  Created by Carson Franklin on 5/1/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct SearchPantry: View {
    @State var navBarHidden = true
    @Binding var searchText: String
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading){
                if searchText == "" {
                    VStack {
                        HStack {
                            Text("Categories")
                                .font(.title)
                                .foregroundColor(Color.white)
                                .padding(.leading)
                                .padding(.top)
                            Spacer()
                        }
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack(alignment: .top, spacing: 20) {
                                ForEach(Store.shared.categories) { category in
                                    CustomNavigationLink(destination: CategoryView(category: category), background: .details, backButtonColor: Color.white) {
                                        CategoryCircleImage(category: category)
                                    }
                                }
                            }
                            .frame(height: 150)
                            .padding(.leading)
                        }
                        .offset(y: -20)
                    }
                    .transition(.opacity)
                }
                
                
                VStack {
                    Divider()
                    .padding(.bottom)
                    // Results header
                    HStack {
                        HStack {
                            if searchText == "" {
                                Text("Expiring")
                                    .font(.title)
                            }
                            Text("Items")
                                .font(.title)
                            
                            if searchText != "" {
                                Button(action: {}) {
                                    Image(systemName: "arrow.up.arrow.down.circle")
                                        .resizable()
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(Color.gray)
                                }
                                .buttonStyle(PlainButtonStyle())
                                .padding(.leading)
                            }
                        }
                        Spacer()
                        if searchText == "" {
                            CustomNavigationLink(destination: ExpiredItemsView(hideDoneBtn: true)) {
                                Text("See All")
                            }
                        }
                        
                    }
                    .padding(.leading)
                    .padding(.trailing)
                    Divider()
                        .padding(.leading)
                        .padding(.trailing)
                    
                    ForEach((1...50).reversed(), id: \.self) { number in
                        VStack {
                            PantryItemRow()
                                .padding(.leading, 20)
                                .padding(.trailing, 20)
                                .padding(.top, 2)
                            Divider()
                                .padding(.leading)
                                .padding(.trailing)
                        }
                    }
                    Spacer()
                }
                .background(Color.white)
                .cornerRadius(10)
                .shadow(radius: 7)
                .padding()
                .animation(.linear)
            }
        }
    }
}
//
//struct SearchPantry_Previews: PreviewProvider {
//    static var previews: some View {
//        SearchPantry()
//    }
//}
