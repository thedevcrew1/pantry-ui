//
//  SearchResultRow.swift
//  Pantry
//
//  Created by Carson Franklin on 5/5/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct SearchResultRow: View {
    var item: PantryItem = PantryItem(id: "foo123", expirationDate: Date(), state: .defualt, product: Product(id: "as;ldfkj", name: "Pork Chop", category: ProductCategory(id: ";aldsfkj", value: "Meat"), experiaitonPeriod: "7 Days"))
    
    var body: some View {
        HStack {
            Text("Pork Chop")
                .font(.headline)
            Spacer()
            Text("7 Days")
                .font(.subheadline)
                .foregroundColor(Color.primary)
        }
    }
}

struct SearchResultRow_Previews: PreviewProvider {
    static var previews: some View {
        SearchResultRow()
    }
}
