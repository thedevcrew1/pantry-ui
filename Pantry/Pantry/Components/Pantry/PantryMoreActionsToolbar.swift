//
//  PantryMoreActionsToolbar.swift
//  Pantry
//
//  Created by Carson Franklin on 5/7/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct PantryMoreActionsToolbar: View {
    
    var doneBtnClicked: () -> Void
    var trashBtnClicked: () -> Void
    
    var body: some View {
        HStack(alignment: .center) {
            
            Button(action: doneBtnClicked) {
                Text("Done")
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
            }
            Spacer()
            Button(action: trashBtnClicked) {
                Image(systemName: "trash")
                                    .imageScale(.large)
                                    .foregroundColor(Color.white)
            }
        }
    }
}

struct PantryMoreActionsToolbar_Previews: PreviewProvider {
    static var previews: some View {
        PantryMoreActionsToolbar(doneBtnClicked: {}, trashBtnClicked: {})
    }
}
