//
//  PantryToolbar.swift
//  Pantry
//
//  Created by Carson Franklin on 4/29/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

class PantryToolbarObserver: ObservableObject {
    
    @Published private(set) var mode = PantryToolbar.Mode.display
    @Published var searchText: String = ""

    func setMode(mode: PantryToolbar.Mode){
        withAnimation{
            switch mode {
            case PantryToolbar.Mode.display:
                self.searchText = ""
                self.dismissKeyboard()
                self.mode = mode
                break
                
            case PantryToolbar.Mode.searching:
                self.dismissKeyboard()
                self.mode = mode
                break
            
            default:
                self.mode = mode
            }
        }
    }
    
    func dismissKeyboard() {
        UIApplication.shared.endEditing()
    }
}

struct PantryToolbar: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @ObservedObject var observer: PantryToolbarObserver
    var expiredItemsBtnClicked: () -> Void
    
    var body: some View {
        HStack {
            Text("Hello World")
            
//            if observer.mode == Mode.searching || observer.mode == Mode.searchingAndTyping {
//                Button(action: {
//                    self.observer.setMode(mode: Mode.display)
//                }) {
//                    Text("Cancel")
//                        .font(.callout)
//                        .foregroundColor(colorScheme == .light ? .black : .white)
//                }
//                .transition(.move(edge: .trailing))
//            }
            
        }
//        .padding(.leading, 2)
//        .padding(.trailing)
    }
    
    enum Mode {
        case searching
        case searchingAndTyping
        case display
    }
}

struct PantryToolbar_Previews: PreviewProvider {
    static var previews: some View {
        PantryToolbar(observer: PantryToolbarObserver(), expiredItemsBtnClicked: {})
    }
}
