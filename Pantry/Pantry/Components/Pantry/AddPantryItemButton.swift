//
//  AddPantryItemButton.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct AddPantryItemButton: View {
    
    var body: some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                Image(systemName: "plus")
                .resizable()
                .frame(width: 20, height: 20)
                .foregroundColor(.white)
                .padding()
                .background(Color.primary)
                .clipShape(Circle())
                .shadow(radius: 7, x:0, y:7)
                .padding(.trailing, 15)
            }
            .padding(.bottom, 5)
        }
    }
}

struct AddPantryItemButton_Previews: PreviewProvider {
    static var previews: some View {
        AddPantryItemButton()
    }
}
