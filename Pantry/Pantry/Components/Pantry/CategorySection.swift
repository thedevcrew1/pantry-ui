//
//  CategorySection.swift
//  Pantry
//
//  Created by Carson Franklin on 5/1/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct CategorySection: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    @State var mode: Mode = Mode.collapsed
    @ObservedObject var selectedProducts: SelectedProductsObservable
    
    private func itemSelected(_ product: Product) -> Void {
        selectedProducts.append(product)
    }
    
    private func itemDeselected(_ product: Product) -> Void {
        self.selectedProducts.remove(product)
    }
    
    var body: some View {
        VStack {
            HStack {
                Image("meat")
                    .resizable()
                    .frame(width:55, height: 55)
                VStack(alignment: .leading) {
                    Text("Meats")
                        .fontWeight(.bold)
                        .foregroundColor(Color.header)
                        .font(Font.system(size: 25, design: .rounded))
                    Text("11 Items")
                        .foregroundColor(Color.gray)
                        .fontWeight(.medium)
                        .font(Font.system(size: 14, design: .rounded))
                }
                .padding(.leading)
                Spacer()
                
                // Collapse button
                if mode == Mode.expanded {
                    Image(systemName: "chevron.up")
                        .imageScale(.small)
                        .foregroundColor(colorScheme == .light ? Color.black : Color.white)
                        .font(Font.title.weight(.medium))
                } else {
                    Image(systemName: "chevron.down")
                        .imageScale(.small)
                        .foregroundColor(colorScheme == .light ? Color.black : Color.white)
                        .font(Font.title.weight(.medium))
                }
            }
            .padding()
            .background(colorScheme == .light ? Color.white : Color.black)
            .onTapGesture {
                withAnimation {
                    self.mode = self.mode == Mode.collapsed ? Mode.expanded : Mode.collapsed
                }
            }
            
            
            // List of Items
            if mode == Mode.expanded {
                Divider()
                ForEach((1...11).reversed(), id: \.self) { number in
                    VStack {
                        PantryProductRow(selectedProducts: self.selectedProducts)
                            .padding(.top, 4)
                            .padding(.bottom, 4)
                            .padding(.leading, 20)
                            .padding(.trailing, 20)
                        Divider()
                    }
                }
                .animation(.spring())
            }
            
        }
        .background(colorScheme == .light ? Color.white : Color.black)
        .cornerRadius(10)
        .shadow(radius: mode == Mode.collapsed ? 5 : 7)
    }
    
    enum Mode {
        case collapsed
        case expanded
    }
}
