//
//  CreatePantryItemsService.swift
//  Pantry
//
//  Created by Carson Franklin on 7/3/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Alamofire
import SwiftyJSON

class   CreatePantryItemsService {
    private var store = Store.shared
    
    
    func execute(items: CreatePantryItemRequests, onCompleteHandler: @escaping (Result<PantryItems, CreatePantryItemError>) -> Void) {
        AF.request("\(AppSettings.baseURL)/item",
            method: .post,
            parameters: items,
            encoder:  JSONParameterEncoder.default
        )
            .validate()
            .responseJSON(completionHandler: { res in
                switch res.result {
                case .success(let resBody):
                    let json = JSON(resBody)
                    self.store.addPantryItems(items: PantryItems(json: json))
                    
                case .failure(let e):
                    guard let statusCode = e.responseCode else {
                        return onCompleteHandler(.failure(.unknownError))
                    }
                    
                    switch statusCode {
                    case 400:
                        onCompleteHandler(.failure(.malformattedData))
                        break
                        
                    case 500:
                        onCompleteHandler(.failure(.serverError))
                        break
                        
                    case 401:
                        onCompleteHandler(.failure(.invalidCredentials))
                        break
                        
                    default:
                        onCompleteHandler(.failure(.unknownError))
                        break
                    }
                    
                }
            })
    }
    
    
    enum CreatePantryItemError: Error {
        case malformattedData
        case serverError
        case invalidCredentials
        case unknownError
    }
}
