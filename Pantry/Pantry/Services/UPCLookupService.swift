//
//  UPCLookupService.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation


class UPCLookupService {
    
    func execute(_ req: UPCLookupRequest, onCompleteHandler: @escaping (Result<UPCLookupResponse, NetworkError>) -> Void) {
        onCompleteHandler(.success(UPCLookupResponse(newProduct: true, name: "Milk", product: nil)))
    }
}
