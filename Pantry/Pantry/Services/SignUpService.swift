//
//  SignUpService.swift
//  Pantry
//
//  Created by Carson Franklin on 4/27/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SignUpService {
    private var store = Store.shared
    
    // MARK: Execute Signup
    // Signs up the user then sets the user in the Store.
    func execute(user:User, _ onCompleteHandler: @escaping (Result<String, SignUpServiceError>) -> Void){
        AF.request("\(AppSettings.baseURL)/user",
            method: .post,
            parameters: user,
            encoder: URLEncodedFormParameterEncoder(destination: .httpBody))
            .validate()
            .responseJSON { res in
                switch res.result {
                case .success(let resBody):
                    let json = JSON(resBody)
                    let jsonUser = json["user"].dictionary!
                     self.store.setUser(user: User(
                        firstName: jsonUser["firstName"]!.string!,
                        lastName: jsonUser["lastName"]!.string!,
                        username: jsonUser["username"]!.string!,
                        email: jsonUser["email"]!.string!,
                        password: user.password))
                    onCompleteHandler(.success("Successfully signed up the user."))
                    
                    break
                    
                case .failure(let e):
                    print("There was an error during signup. Localized description: \(e.localizedDescription)")
                    
                    guard let statusCode = e.responseCode else {
                        return onCompleteHandler(.failure(.unknownError))
                    }
                    
                    switch statusCode {
                    case 400:
                        onCompleteHandler(.failure(.malformattedData))
                        break
                        
                    case 401:
                        onCompleteHandler(.failure(.usernameInUse))
                        break
                        
                    case 500:
                        onCompleteHandler(.failure(.serverError))
                        break
                        
                    default:
                        onCompleteHandler(.failure(.unknownError))
                        break
                        
                    }
                }
        }
    }
    
    
    // MARK: SignUpServiceError
    enum SignUpServiceError: Error {
        case malformattedData
        case usernameInUse
        case serverError
        case unknownError
    }
}
