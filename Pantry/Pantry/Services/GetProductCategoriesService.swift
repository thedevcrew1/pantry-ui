//
//  GetProductCategoriesService.swift
//  Pantry
//
//  Created by Carson Franklin on 7/14/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class GetProductCategoriesService {
    
    func execute(_ onCompleteHandler: @escaping (Result<[ProductCategory], NetworkError>) -> Void) -> Void {
        AF.request("\(AppSettings.baseURL)/categories")
        .validate()
            .responseJSON(completionHandler: { res in
                switch res.result {
                case .success(let resBody):
                    let json = JSON(resBody)
                    guard let jsonCategories = json.array else {
                        onCompleteHandler(.failure(NetworkError.malformedResponse))
                        return
                    }
                    var res: [ProductCategory] = []
                    
                    do {
                        for jsonCategory in jsonCategories {
                           try res.append(ProductCategory(json: jsonCategory))
                        }
                    } catch {
                        onCompleteHandler(.failure(error as? NetworkError ?? NetworkError.unknownError))
                        return
                    }
                    
                    onCompleteHandler(.success(res))
                    
                    break
                    
                case .failure(let e):
                    onCompleteHandler(.failure(handleNetworkFailure(e: e)))
                    break
                }
        })
    }
}
