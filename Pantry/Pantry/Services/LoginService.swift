//
//  LoginService.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class LoginService {
    private var store = Store.shared
    private var onCompleteHandler: (Result<String, LoginServiceError>) -> Void
    
    init(_ onCompleteHandler: @escaping (Result<String, LoginServiceError>) -> Void) {
        self.onCompleteHandler = onCompleteHandler
    }
    
    func execute(user: User) -> Void {
        AF.request("\(AppSettings.baseURL)/user/login",
            method: .post,
            parameters: user,
            encoder: URLEncodedFormParameterEncoder(destination: .httpBody))
        .validate()
        .responseJSON(completionHandler: handleLoginResponse)
    }
    
    func handleLoginResponse(res: AFDataResponse<Any>) {
        switch res.result {
        case .success(let resBody):
            let json = JSON(resBody)
            let jsonUser = json["user"].dictionary!
            
            self.store.setUser(user: User(
                firstName: jsonUser["firstName"]!.string!,
                lastName: jsonUser["lastName"]!.string!,
                username: jsonUser["username"]!.string!,
                email: jsonUser["email"]!.string!,
                password: ""))
            
            onCompleteHandler(.success("Successfully logged in."))
            
            break
            
        case .failure(let e):
            guard let statusCode = e.responseCode else {
                return onCompleteHandler(.failure(.unknownError))
            }
            
            switch statusCode {
            case 400:
                onCompleteHandler(.failure(.malformattedData))
                break
                
            case 500:
                onCompleteHandler(.failure(.serverError))
                break
                
            case 401:
                onCompleteHandler(.failure(.invalidCredentials))
                break
                
            default:
                onCompleteHandler(.failure(.unknownError))
                break
            }
        }
    }
    
    // MARK: LoginServicError
    enum LoginServiceError: Error {
        case malformattedData
        case serverError
        case invalidCredentials
        case unknownError
    }
}
