//
//  NetworkError.swift
//  Pantry
//
//  Created by Carson Franklin on 7/14/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation


enum NetworkError: Error {
    case malformattedData
    case usernameInUse
    case invalidCredentials
    case serverError
    case unknownError
    case malformedResponse
}
