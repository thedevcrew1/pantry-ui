//
//  Utils.swift
//  Pantry
//
//  Created by Carson Franklin on 7/14/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation
import Alamofire

func handleNetworkFailure(e: AFError) -> NetworkError {
    guard let statusCode = e.responseCode else {
        return NetworkError.unknownError
    }
    
    switch statusCode {
    case 400:
        return NetworkError.malformattedData
        
    case 500:
        return NetworkError.serverError
        
    case 401:
        return NetworkError.invalidCredentials
        
    default:
        return NetworkError.unknownError
    }
}
