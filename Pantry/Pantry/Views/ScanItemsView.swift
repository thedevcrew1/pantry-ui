//
//  ScanItemsView.swift
//  Pantry
//
//  Created by Carson Franklin on 5/19/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI
import AVFoundation

struct ScanItemsView: View {
    @ObservedObject var scanItemsObservable: ScanItemsObservable
    
    var body: some View {
        
        VStack {
            
            CodeScannerView(codeTypes: AppSettings.supportedUPCs, completion: scanItemsObservable.scanCompleted)
                .frame(maxWidth: .infinity, maxHeight: 200)
                .cornerRadius(10)
                .padding(.trailing)
                .padding(.leading)
            
            VStack(alignment: .leading) {
                
                if scanItemsObservable.scannedItems.count < 1 {
                    Spacer()
                    ScanItemsViewInstructionPrompt()
                    
                } else {
                    ScanItemsViewScannedItemsList(scanItemsObservable: scanItemsObservable)
                        .padding()
                        .cornerRadius(10)
                        .edgesIgnoringSafeArea(.bottom)
                        .transition(.opacity)
                }
                
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        
    }
    
}

struct ScanItemsView_Previews: PreviewProvider {
    static var previews: some View {
        ScanItemsView(scanItemsObservable: ScanItemsObservable(addItemsObservable: AddItemsObservable(), newProductSheetDisplayed: {}))
    }
}
