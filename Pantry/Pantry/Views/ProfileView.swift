//
//  ProfileView.swift
//  Pantry
//
//  Created by Carson Franklin on 4/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    @State var user: User = (Store.shared.user != nil) ? Store.shared.user! : User(firstName: "", lastName: "", username: "", email: "", password: "")
    
    var doneBtnClicked: () -> Void
    var logoutBtnClicked: () -> Void
    
    var body: some View {
        ZStack {
            VStack {
                HStack {
                    ViewTitle(text: "Profile", color: Color.header)
                        .padding(.leading)
                    Spacer()
                }
                .padding(.top)
                
                Form {
                    Section(header: Text("Username").font(.caption)) {
                        TextField("Username", text: $user.username)
                    }
                    .padding(.top)
                    
                    Section(header: Text("Email").font(.caption)) {
                        TextField("Email", text: $user.email)
                    }
                    
                    Section(header: Text("Name").font(.caption)) {
                        TextField("First Name", text: $user.firstName)
                        TextField("Last Name", text: $user.lastName)
                    }
                    
                }
                .padding(.bottom)
                
                LogoutButton(action: { self.logoutBtnClicked() })
            }
            .padding(.top, 20)
            
            
            VStack {
                
                HStack {
                    Spacer()
                    Button(action: doneBtnClicked) {
                        Text("Done")
                            .foregroundColor(Color.primary)
                    }
                }
                .padding(.trailing)
                .padding(.top)
                .background(Color.white)
                Spacer()
            }
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(doneBtnClicked: {}, logoutBtnClicked: {})
    }
}
