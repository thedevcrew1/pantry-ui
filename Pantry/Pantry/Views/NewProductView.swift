//
//  NewProductView.swift
//  Pantry
//
//  Created by Carson Franklin on 7/15/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct NewProductView: View {
    @ObservedObject var newProductObservable: NewProductObservable
    
    var body: some View {
        ZStack {
            VStack {
                NewProductViewTitle()
                    .padding(.leading)
                    .padding(.top)
                NewProductViewExplinationText()
                    .padding(4.0)
                    .padding(.top)
                NewProductViewForm(newProductObservable: newProductObservable)
                
                NewProductViewSaveButton(action: newProductObservable.save)
            }
        }
    }
}

struct NewProductView_Previews: PreviewProvider {
    static var previews: some View {
        NewProductView(newProductObservable: NewProductObservable(product: Product(id: UUID().uuidString, name: "", category: ProductCategory(id: UUID().uuidString, value: ""), experiaitonPeriod: "")))
     }
}
