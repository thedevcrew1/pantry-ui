//
//  CategoryView.swift
//  Pantry
//
//  Created by Carson Franklin on 5/11/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct CategoryView: View {
    
    let category: ProductCategory
    
    var body: some View {
        ScrollView {
            VStack {
                HStack {
                    Image(UIImage(named: category.value) != nil ? category.value : "supermarket")
                        .resizable()
                        .frame(width:40, height: 40)
                    ViewTitle(category.value)
                    Spacer()
                }
                .padding(.leading)
                .cornerRadius(10)
                
                VStack {
                    Divider()
                    ForEach((1...11).reversed(), id: \.self) { number in
                        VStack {
                            PantryProductRow()
                                .padding()
                            Divider()
                        }
                    }
                }
                .background(Color.white)
                .cornerRadius(10)
            .shadow(radius: 7)
            .padding()
            }
        }
    }
}

struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView(category: ProductCategory(id: "a;lsdkfj", value: "Milk"))
    }
}
