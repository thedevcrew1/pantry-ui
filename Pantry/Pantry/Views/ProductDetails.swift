//
//  ProductDetails.swift
//  Pantry
//
//  Created by Carson Franklin on 5/5/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ProductDetails: View {
    
    var product:Product
    
    // Formats the product items into a 2D array so we can loop over them easily.
    var twoDPoductItems: [[PantryItem]] {
        get {
            var res: [[PantryItem]] = []
            
            for i in stride(from: 0, to: (product.items.count), by: 2){
                res.append([product.items[i], product.items[i + 1]])
            }
            
            return res
        }
    }
    
    var body: some View {
        VStack {
            ScrollView {
                // Header
                HStack {
                    Image("meat")
                        .resizable()
                        .frame(width: 50, height: 50)
                    VStack(alignment: .leading) {
                        ViewTitle(text: product.name)
                        Text("5 items, 2 Expired")
                            .foregroundColor(Color.white)
                    }
                    
                    Spacer()
                    
                }
                .padding(.leading, 16)
                .padding(.trailing, 16)
                .padding(.bottom)
                
                // List
                ForEach(0..<twoDPoductItems.endIndex) { i in
                    HStack {
                        ForEach(self.twoDPoductItems[i]) { item in
                            PantryItemDetails(item: item, productName: self.product.name, expired: false)
                                .padding(8)
                        }
                    }
                    .padding(.leading, 8)
                    .padding(.trailing, 8)
                }
                if self.twoDPoductItems.count > 0 {
                    ItemDivider()
                    .padding(16)
                }
                
                // List
                ForEach(0..<twoDPoductItems.endIndex) { i in
                    HStack {
                        ForEach(self.twoDPoductItems[i]) { item in
                            PantryItemDetails(item: item, productName: self.product.name, expired: true)
                                .padding(8)
                        }
                    }
                    .padding(.leading, 8)
                    .padding(.trailing, 8)
                }
                Spacer()
            }
            
        }
    }
    
    struct ItemDivider: View {
        let color: Color = .gray
        let width: CGFloat = 1
        var body: some View {
            Rectangle()
                .fill(color)
                .frame(height: width)
                .edgesIgnoringSafeArea(.horizontal)
        }
    }
}

struct PantryItemDetails_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetails(product: Product(id: "foo1", name: "Pork Chop", category: ProductCategory(id: "foo123", value: "Meat"), experiaitonPeriod: "1 Week", items: [PantryItem(id: "foo", expirationDate: Date(), state: PantryItemState.defualt)]))
    }
}

