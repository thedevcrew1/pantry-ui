//
//  PantryItemDetails.swift
//  Pantry
//
//  Created by Carson Franklin on 5/5/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct PantryItemDetails: View {
    
    var item:PantryItem
    
    var body: some View {
        VStack {
            HStack {
                Image("meat")
                .resizable()
                .frame(width: 50, height: 50)
                VStack(alignment: .leading) {
                    ViewTitle(text: item.product.name)
                    Text("Quantity 10")
                        .foregroundColor(Color.gray)
                }
                
                Spacer()
                
            }
            .padding(.leading, 30)
            
            ScrollView {
                ForEach(0..<10) { _ in
                    PantryItemDetailsItem(item: self.item)
                        .padding(.leading)
                        .padding(.trailing)
                        .padding(.top)
                }
                Spacer()
            }

        }
    }
}

struct PantryItemDetails_Previews: PreviewProvider {
    static var previews: some View {
        PantryItemDetails(item: PantryItem(id: "foo", product: Product(id: "foo1", name: "Pork Chop", category: Category(id: "foo123", value: "Dairy"), experiaitonPeriod: "1 Week"), expirationDate: "Jan 12", state: PantryItemState.defualt))
    }
}
