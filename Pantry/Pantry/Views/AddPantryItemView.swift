//
//  AddPantryItemView.swift
//  Pantry
//
//  Created by Carson Franklin on 4/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI


struct AddPantryItemView: View {
    var cancelBtnClicked: () -> Void
    @ObservedObject var addItemsObservable: AddItemsObservable
    @ObservedObject var scanItemsObservable: ScanItemsObservable
    @ObservedObject var searchProductsObservable = SearchBarObservable(placeHolder: "Search Products")
    @ObservedObject var searchCategoryObservable = SearchBarObservable()
    
    init(cancelBtnClicked: @escaping () -> Void) {
        let addItemsObservable = AddItemsObservable()
        self.addItemsObservable = addItemsObservable
        self.scanItemsObservable = ScanItemsObservable(addItemsObservable: addItemsObservable, newProductSheetDisplayed: addItemsObservable.displayNewProductSheet)
        self.cancelBtnClicked = cancelBtnClicked
    }
    
    var body: some View {
        ZStack {
            AddPantryItemBackground()
            
            VStack {
                if addItemsObservable.mode == .display || addItemsObservable.mode == .searchingProducts {
                    ScrollView {
                        AddPantryItemDisplayView(searchBarObservable: searchProductsObservable, categoryTileClicked: addItemsObservable.categoryTileClicked)
                            .transition(.move(edge: .leading))
                            .padding(.bottom)

                        if addItemsObservable.mode == .searchingProducts {
                            ProductSearchResults(addItemsObservable: addItemsObservable)
                                .transition(.move(edge: .bottom))
                        }

                        if addItemsObservable.mode == .display {
                            VStack {
                                Spacer()
                                ScanBarCodesButton(action: addItemsObservable.scanItemsBtnClicked)
                                    .padding(.top)
                                    .padding(.bottom, 90)
                                    .transition(.move(edge: .bottom))
                            }
                        }
                    }
                }
                
                
//                 MARK: Category View
                if addItemsObservable.mode == .viewCategory {
                    ScrollView{
                        AddPantryItemCategory(category: addItemsObservable.selectedCategory, addItemsObservable: addItemsObservable, searchBarObservable: searchCategoryObservable)
                    }
                    .transition(.move(edge: .trailing))
                }

                if addItemsObservable.mode == .scanItems {
                    ScanItemsView(scanItemsObservable: scanItemsObservable)
                        .transition(.move(edge: .trailing))
                }
            }
            .padding(.top, 50)


            AddPantryItemToolbar(mode: $addItemsObservable.mode, cancelBtnClicked: cancelBtnClicked, scanItemsBtnClicked: addItemsObservable.scanItemsBtnClicked)

            if (addItemsObservable.items.count > 0) {
                AddItemsButton(addItemsObservable: addItemsObservable, action: addItemsObservable.addItemsBtnClicked)
                    .transition(.move(edge: .bottom))
                    .animation(.spring())
            }
        }
        .sheet(isPresented: $addItemsObservable.sheetShown, onDismiss: addItemsObservable.sheetDissmissed) {
            if self.addItemsObservable.mode == .newProductSheetShown {
                NewProductView(newProductObservable: self.scanItemsObservable.newProductObservable)
            } else {
                ConfirmAddItemsView(addItemsObservable: self.addItemsObservable, saveBtnClicked: self.addItemsObservable.itemsConfirmed)
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        .onAppear{
            self.searchProductsObservable.registerIsEditingUpdatedHandler(self.addItemsObservable.isSearchProductsChanged)
        }
    }
    
    
    
    
}

struct AddPantryItemView_Previews: PreviewProvider {
    static var previews: some View {
        AddPantryItemView(cancelBtnClicked: {})
    }
}
