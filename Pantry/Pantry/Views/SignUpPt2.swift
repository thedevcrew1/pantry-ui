//
//  SignUpPt2.swift
//  Pantry
//
//  Created by Carson Franklin on 4/27/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI
import Validator

struct SignUpPt2: View {
    @Binding var user: User
    @ObservedObject var navigationObservable = NavigationObserver.shared
    @State var mode: Mode = Mode.display
    @State var errorMessage = ""
    @State var emailMode: FieldMode = .display
    @State var usernameMode: FieldMode = .display
    @State var passwordMode: FieldMode = .display
    
    var body: some View {
        
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text("Nice to meet you \(self.user.firstName),")
                        .frame(height: 30)
                        .font(Font.system(size: 30, design: .rounded))
                        .multilineTextAlignment(.leading)
                        .padding(.bottom, 25.0)
                    
                    // MARK: Email Field
                    VStack(alignment: .leading) {
                        TextField("Email", text: self.$user.email, onEditingChanged: emailTextChanged)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .autocapitalization(.none)
                            .frame(minWidth: 0, maxWidth: 250)
                            
                        
                        // MARK: Email Validation Error Message
                        if emailMode == .invalid {
                            ErrorMessage("Please enter a valid email")
                        }
                    }
                    .padding(.bottom)
                    
                    // MARK: Username Field
                    VStack(alignment: .leading) {
                        TextField("Username", text: self.$user.username, onEditingChanged: usernameTextChanged)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .autocapitalization(.none)
                            .frame(minWidth: 0, maxWidth: 250)
                            
                        
                        // MARK: Email Validation Error Message
                        if usernameMode == .invalid {
                            ErrorMessage("Username must be between 5 - 15 characters")
                        }
                    }
                    .padding(.bottom)
                    
                    // MARK: Password Field
                    SecureField("Password", text: self.$user.password, onCommit: passwordCommitted)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .textContentType(.password)
                        .autocapitalization(.none)
                        .frame(minWidth: 0, maxWidth: 250)
                    
                    // MARK: Password Validation Error Message
                    if passwordMode == .invalid {
                        ErrorMessage("Password must be at least 8 characters long")
                    }
                }
                
                Spacer()
            }
            .padding(.leading)
            
            Spacer()
            
            // MARK: Signup Error Message
            if mode == Mode.signupError {
                Text(errorMessage)
                .font(.headline)
                .foregroundColor(Color.pink)
                    .transition(.move(edge: .bottom))
                .padding()
            }
            
            // MARK: Signup Button
            Button(action: signupBtnClicked) {
                VStack(alignment: .center) {
                    Text("Sign Up")
                    .font(.headline)
                    .foregroundColor(Color.white)
                    .padding()
                }
            }
            .frame(minWidth:0, maxWidth: 300)
            .background(Color.primary)
            .cornerRadius(20)
        }
    }
    
    struct ErrorMessage: View {
        let message: String
        
        init(_ message: String) {
            self.message = message
        }
        
        var body: some View {
            Text(message)
            .font(.caption)
            .foregroundColor(Color.red)
            .transition(.opacity)
        }
    }
    
    // MARK: Funcitons
    
    /// Validates the fields and modifies the view's mode accordingly and plays an error haptic when necessary.
    func setModeBasedOnFieldsValidity() -> Void {
        withAnimation {
            mode = isPasswordValid() && isUsernameValid() && isEmailValid() ? .fieldsValid : .display
        }
    }
    
    func isEmailValid() -> Bool {
        let emailRule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidationErrors.emailInvalid)
        let emailValidRes = self.user.email.validate(rule: emailRule)
        
        switch emailValidRes {
        case .valid:
            return true
        case .invalid(_):
            return false
        }
    }
    
    func isPasswordValid() -> Bool {
        let passwordRule = ValidationRuleLength(min: 8, error: ValidationErrors.passwordToSmall)
        
        
        let passwordValidRes = self.user.password.validate(rule: passwordRule)
        if passwordValidRes == ValidationResult.valid {
            return true
        }
        
        return false
    }
    
    func isUsernameValid() -> Bool {
        let validationRes = self.user.username.validate(rule: ValidationRuleLength(min: 5, max: 15, error: ValidationErrors.usernameInvalid))
        
        switch validationRes {
        case .valid:
            return true
        case .invalid(_):
            return false
        }
    }
    
    func emailTextChanged(_ isEditing: Bool) {
        if isEditing {
            return
        }
        
        if isEmailValid() {
            emailMode = .valid
        } else {
            emailMode = .invalid
        }
    }
    
    func usernameTextChanged(_ isEditing: Bool) {
        if isEditing {
            return
        }
        
        if isUsernameValid() {
            usernameMode = .valid
        } else {
            usernameMode = .invalid
        }
    }
    
    func passwordCommitted() {
        if isPasswordValid() {
            passwordMode = .valid
        } else {
            passwordMode = .invalid
        }
    }
    
    func signupBtnClicked() -> Void {
        setModeBasedOnFieldsValidity() // Set mode by checking if the fields are valid.
        
        if mode != .fieldsValid {
            HapticEngine.shared.errorHaptic()
            return
        }
        
        self.mode = Mode.loading
        let signUpService = SignUpService()
        signUpService.execute(user: self.user, self.onSignupComplete)
    }
    
    func onSignupComplete(_ result: Result<String, SignUpService.SignUpServiceError>) -> Void {
        DispatchQueue.main.async {
            switch result {
            case .success(_):
                print("Success.")
                self.navigationObservable.navigateToRoot() // Navigate back to Root View.
                break
            case .failure(let error):
                switch error {
                case .usernameInUse:
                    self.mode = .signupError
                    self.errorMessage = "Sorry, that username is already taken. Please pick a different one."
                    break
                    
                case .malformattedData:
                    self.mode = .signupError
                    self.errorMessage = "Sorry, some of the data must have been entered incorrectly, please try again."
                    break
                    
                case .serverError:
                    self.mode = .signupError
                    self.errorMessage = "Sorry, there was an error on our side. Our devs are tracking down the problem. Please try again later."
                    break
                    
                case .unknownError:
                    self.mode = .signupError
                    self.errorMessage = "An unkown has error occurred, if the problem persists please notify us."
                    break
                }
                
                HapticEngine.shared.errorHaptic()
            }
        }
    }
    
    enum ValidationErrors: String, ValidationError {
        case emailInvalid = "Email address is invalid"
        case passwordToSmall = "Password must be at least 8 characters long."
        case usernameInvalid = "Username must be between 5 to 15 characters long."
        var message: String { return self.rawValue }
    }
    
    enum Mode {
        case display
        case loading
        case emailInvalid
        case passwordInvalid
        case emailAndPasswordInvalid
        case fieldsValid
        case signupError
    }
    
    enum FieldMode {
        case display
        case valid
        case invalid
    }
}

struct SignUpPt2_Previews: PreviewProvider {
    static var previews: some View {
        SignUpPt2(user: .constant(User(firstName: "John", lastName: "Doe", username: "", email: "", password: "")))
    }
}
