//
//  LoginView.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct LoginView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var user: User = User(firstName: "", lastName: "", username: "", email:"", password: "")
    @ObservedObject var navigationObservable = NavigationObserver.shared
    @State var mode: Mode = .display
    
    func onLoginComplete(res: Result<String, LoginService.LoginServiceError>) -> Void {
        switch res {
        case .success(_):
            navigationObservable.navigateToRoot()
            
        case(.failure(let e)):
            switch e {
            case .invalidCredentials:
                mode = .invalidCredentials
            default:
                mode = .unkownError
            }
        }
    }
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text("Welcome back!")
                        .frame(height: 30)
                        .font(Font.system(size: 30, design: .rounded))
                        .multilineTextAlignment(.leading)
                        .padding(.bottom, 25.0)
                        .padding(.leading)
                    
                    // Email Field
                    TextField("Username", text: self.$user.username) .textFieldStyle(RoundedBorderTextFieldStyle())
                        .autocapitalization(.none)
                        .frame(minWidth: 0, maxWidth: 250)
                        .padding(.bottom)
                        .padding(.leading)
                    
                    // Password Field
                    SecureField("Password", text: self.$user.password).textFieldStyle(RoundedBorderTextFieldStyle())
                        .textContentType(.password)
                        .autocapitalization(.none)
                        .frame(minWidth: 0, maxWidth: 250)
                        .padding(.leading)
                }
                
                Spacer()
            }
            
            Spacer()
            Button(action: {
                let loginServic = LoginService(self.onLoginComplete)
                loginServic.execute(user: self.user)
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Text("Login")
                    .font(.headline)
                    .foregroundColor(Color.white)
                    .padding()
            }
            .frame(minWidth:0, maxWidth: 300)
            .background(Color.primary)
            .cornerRadius(20)
        }
    }
    
    enum Mode {
        case display
        case invalidCredentials
        case unkownError
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
