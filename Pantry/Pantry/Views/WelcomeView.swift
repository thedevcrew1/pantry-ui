//
//  WelcomeView.swift
//  Pantry
//
//  Created by Carson Franklin on 4/25/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct WelcomeView: View {
    var body: some View {
        HStack {
            VStack {
                Spacer()
                Image("rasberry")
                    .resizable()
                    .frame(width: 100, height: 100)
                Text("Welcome to")
                    .frame(height: 30)
                    .font(Font.system(size: 30, design: .rounded))
                    .multilineTextAlignment(.leading)
                Text("Rasberry")
                    .frame(height: 50)
                    .font(Font.system(size: 50, design: .rounded))
                    .multilineTextAlignment(.leading)
                    .padding(.bottom, 75.0)
                CustomNavigationLink(destination: SignUpPt1()) {
                    Text("Start stocking")
                        .font(.headline)
                        .foregroundColor(Color.white)
                        .padding()
                }
                .frame(minWidth:0, maxWidth: 300)
                .background(Color.primary)
                .cornerRadius(20)
                Spacer()
                
                HStack {
                    Spacer()
                    CustomNavigationLink(destination: LoginView()){
                        Text("Already have an account?")
                            .foregroundColor(.primary)
                        Image(systemName: "chevron.right")
                            .padding(.trailing)
                            .foregroundColor(.primary)
                    }
                    
                }
            }
            
        }
        
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
