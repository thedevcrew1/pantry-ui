//
//  SignUp.swift
//  Pantry
//
//  Created by Carson Franklin on 4/27/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI
import Validator

struct SignUpPt1: View {
    @State var user: User = User(firstName: "", lastName: "", username: "", email: "", password: "")
    
    @State var firstNameMode: FieldMode = .display
    @State var lastNameMode: FieldMode = .display
    
    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text("What should we call you?")
                        .frame(height: 30)
                        .font(Font.system(size: 30, design: .rounded))
                        .multilineTextAlignment(.leading)
                        .padding(.bottom, 25.0)
                        .padding(.leading)
                    
                    // MARK: First Name Text Field
                    TextField("First Name", text: self.$user.firstName, onEditingChanged: firstNameChanged)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(minWidth: 0, maxWidth: 250)
                        
                    
                    // MARK: First Name Validation Error Message
                    if firstNameMode == .invalid {
                        Text("First name is required and must be less than 15 characters")
                            .font(.caption)
                            .foregroundColor(Color.red)
                            .transition(.opacity)
                            .padding(.bottom)
                    }
                    
                    // MARK: Last Name Text Field
                    TextField("Last Name", text: self.$user.lastName, onEditingChanged: lastNameChanged)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .frame(minWidth: 0, maxWidth: 250)
                    
                    // MARK: Last Name Validation Error Message
                    if lastNameMode == .invalid {
                        Text("Last name is required and must be less than 15 characters")
                            .font(.caption)
                            .foregroundColor(Color.red)
                            .transition(.opacity)
                    }
                }
                
                Spacer()
            }
            .padding(.leading)
            
            Spacer()
            VStack {
                if firstNameMode == .valid && lastNameMode == .valid {
                    CustomNavigationLink(destination: SignUpPt2(user: $user)) {
                        Text("Continue")
                            .font(.headline)
                            .foregroundColor(Color.white)
                            .padding()
                    }
                    .frame(minWidth:0, maxWidth: 300)
                    .background(Color.primary)
                    .cornerRadius(20)
                    .transition(.opacity)
                    
                } else {
                    Text("Continue")
                    .font(.headline)
                    .foregroundColor(Color.white)
                    .padding()
                    .frame(minWidth:0, maxWidth: 300)
                    .background(Color.gray)
                    .cornerRadius(20)
                    .transition(.opacity)
                }
            }
        }
    }
    
    func validateName(_ name: String) -> Bool {
        
        let validationResult = name.validate(rule: ValidationRuleLength(min: 1, max: 15, error: ValidationErrors.nameInvalid))
        if validationResult == ValidationResult.valid {
            return true
        }
        
        return false
    }
    
    func firstNameChanged(_ isEditing: Bool) -> Void {
        if isEditing {
            return
        }
        
        withAnimation {
            firstNameMode = validateName(user.firstName) ? .valid : .invalid
        }
    }
    
    func lastNameChanged(_ isEditing: Bool) -> Void {
        if isEditing {
            return
        }
        
        withAnimation {
            lastNameMode = validateName(user.lastName) ? .valid : .invalid
        }
    }
    
    enum FieldMode {
        case display
        case valid
        case invalid
    }
    
    enum ValidationErrors: String, ValidationError {
        case nameInvalid = "First name is invalid."
        var message: String { return self.rawValue }
    }
}

struct SignUpPt1_Previews: PreviewProvider {
    static var previews: some View {
        SignUpPt1()
    }
}
