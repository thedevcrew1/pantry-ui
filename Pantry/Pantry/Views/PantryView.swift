//
//  PantryView.swift
//  Pantry
//
//  Created by Carson Franklin on 4/28/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct PantryView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @ObservedObject var searchBarObservable = SearchBarObservable()
    @State var headerDisapeared: Bool = false
    @State var navigationBarStyle = NavigationBarItem.TitleDisplayMode.large
    @State var sheetShown = false
    @State var mode = Mode.display
    @ObservedObject var selectedProducts = SelectedProductsObservable()
    
    
    func expiredItemsDoneBtnClicked() -> Void {
        self.sheetShown.toggle()
        self.mode = Mode.display
    }
    
    func expiredItemsBtnClicked() -> Void {
        self.sheetShown.toggle()
        self.mode = Mode.expiredItemsSheetShown
    }
    
    func profileBtnClicked() -> Void {
        self.mode = Mode.profileSheetShown
        self.sheetShown.toggle()
    }
    
    func onSheetDismissed() -> Void {
        self.mode = Mode.display
    }
    
    func addPantryItemBtnClicked() -> Void {
        self.mode = Mode.addItemSheetShown
        self.sheetShown.toggle()
    }
    
    func addPantryItemCancelBtnClicked() -> Void {
        self.sheetShown.toggle()
        self.mode = Mode.display
    }
    
    func doneBtnClicked() -> Void {
        withAnimation {
            self.mode = Mode.display
            self.selectedProducts.clear()
        }
    }
    
    func trashBtnClicked() -> Void {
        //TODO: Impliment
    }
    
    func scanItemsBtnClicked() -> Void {
        self.mode = Mode.scanItemsSheetShown
    }
    
    func scanItemsCancelBtnClicked() -> Void {
        self.sheetShown.toggle()
        self.mode = Mode.display
    }
    
    var body: some View {
        ZStack {
            BackgroundImage()
            VStack {
                VStack(alignment: .center) {
                    
                    if selectedProducts.products.count < 1 {
                        SearchBar(searchBarObservable: searchBarObservable)
                            .padding(.leading, 10)
                            .padding(.trailing, 10)
                            .padding(.top)
                            .transition(.asymmetric(insertion: .move(edge: .bottom), removal: .scale))
                    } else {
                        PantryMoreActionsToolbar(doneBtnClicked: doneBtnClicked, trashBtnClicked: trashBtnClicked)
                            .padding(.top)
                            .padding(.leading, 25)
                            .padding(.trailing, 25)
                            .transition(.scale)
                        Divider()
                            .foregroundColor(Color.white)
                            .offset(y: 7)
                            
                    }
                    
                    if !searchBarObservable.isEditing {
                        ScrollView {
                            PantryHeader(profileBtnClicked: self.profileBtnClicked, expiredItemsBtnClicked: self.expiredItemsBtnClicked)
                                .padding(.bottom, 20)
                                .padding(.leading, 40)
                                .padding(.trailing, 36)
                                .padding(.top, 15)
                            
                            
                            ForEach((1...50).reversed(), id: \.self) { number in
                                VStack {
                                    CategorySection(selectedProducts: self.selectedProducts)
                                        .padding(.bottom)
                                        .padding(.leading, 25)
                                        .padding(.trailing, 25)
                                }
                            }
                        }
                        .transition(.opacity)
                    } else {
                        SearchPantry(searchText: self.$searchBarObservable.text)
                            .transition(.asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .trailing)))
                    }
                    
                }
                
                
                Spacer()
                
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            
            CustomNavigationLink(destination: AddPantryItemView(cancelBtnClicked: {}), hideBackButton: true) {
                AddPantryItemButton()
                .padding(.trailing)
                .padding(.bottom)
            }
        }
        .sheet(isPresented: $sheetShown, onDismiss: { self.onSheetDismissed() }) {
            if self.mode == Mode.expiredItemsSheetShown {
                ExpiredItemsView(doneBtnClicked: self.expiredItemsBtnClicked)
            } else if self.mode == Mode.profileSheetShown {
                ProfileView(doneBtnClicked: { self.sheetShown.toggle() }, logoutBtnClicked: { self.sheetShown.toggle() })
            } else if self.mode == Mode.addItemSheetShown {
                AddPantryItemView(cancelBtnClicked: self.addPantryItemCancelBtnClicked)
            }
        }
        .edgesIgnoringSafeArea(.bottom)
        
    }
    
    enum Mode {
        case display
        case expiredItemsSheetShown
        case profileSheetShown
        case moreActions
        case addItemSheetShown
        case scanItemsSheetShown
    }
}

struct PantryView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            PantryView()
            
            PantryView()
                .environment(\.colorScheme, .dark)
        }
    }
}

