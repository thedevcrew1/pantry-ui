//
//  ContentView.swift
//  Pantry
//
//  Created by Carson Franklin on 4/25/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI
import AVFoundation

struct ContentView: View {
    @State private var selection = 0
    @ObservedObject var store = Store.shared
    @ObservedObject var notificationObservable = appNotificationObservable

    var body: some View {
        ZStack {
            VStack {
                if store.user?.firstName == nil {

                    WelcomeView()

                } else {
                    PantryView()
                }
            }
            .navigationBarBackButtonHidden(true)
            
            NotificationView(notificationObservable: notificationObservable)
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
