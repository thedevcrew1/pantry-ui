//
//  ExpiredItemsView.swift
//  Pantry
//
//  Created by Carson Franklin on 4/30/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import SwiftUI

struct ExpiredItemsView: View {
    var hideDoneBtn: Bool = false
    var doneBtnClicked: (() -> Void)?
    @State private var expiredInterval: String = "10 Days"
    
    var body: some View {
        ZStack {
            AddPantryItemBackground()
            ScrollView {
                
                
                VStack {
                    HStack {
                        VStack(alignment: .leading) {
                            ViewTitle("Expiring Items", color: Color.black)
                            ItemsExpiringWithinSetting(interval: $expiredInterval)
                            Text("0 items")
                                .font(.subheadline)
                                .foregroundColor(Color.gray)
                        }
                        Spacer()
                    }
                    .padding(.leading)
                    .padding(.top, 30)
                    VStack {
                        ForEach((1...50).reversed(), id: \.self) { number in
                            VStack {
                                PantryItemRow()
                                    .padding(.leading)
                                    .padding(.trailing)
                                Divider()
                                    .padding(.leading)
                                    .padding(.trailing)
                            }
                        }
                    }
                    .padding(.top)
                    .background(Color.white)
                    .cornerRadius(10)
                    .shadow(radius: 4)
                    .padding(.leading)
                    .padding(.trailing)
                }
            }
            
            
            VStack {
                if !hideDoneBtn {
                    HStack {
                        Spacer()
                        Button(action: {
                            if self.doneBtnClicked != nil {
                                self.doneBtnClicked!()
                            }
                        }) {
                            Text("Done")
                                .foregroundColor(Color.black)
                        }
                    }
                    .padding(.trailing)
                    .padding(.top)
                    .background(Color.white)
                }
                Spacer()
            }
        }
    }
    
    // MARK: Sub-components
    
    struct ItemsExpiringWithinSetting: View {
        @Binding var interval: String
        @State var modifyIntervalSheetShown: Bool = false
        
        
        var body: some View {
            HStack {
                Text("Expiring within ")
                    .font(.subheadline)
                    .foregroundColor(Color.gray)
                +
                    Text(interval)
                        .font(.subheadline)
                        .foregroundColor(Color.primary)
                        .fontWeight(.bold)
            }
            .onTapGesture {
                self.tapped()
            }
            .sheet(isPresented: $modifyIntervalSheetShown) {
                EditIntervalSheet(interval: self.$interval, selfShown: self.$modifyIntervalSheetShown)
            }
        }
        
        // MARK: ItemsExpiringWithinSetting Methods
        func tapped() {
            self.modifyIntervalSheetShown = true
        }
        
        // MARK: ItemsExpiringWithinSetting Sub-Components
        
        struct EditIntervalSheet: View {
            @Binding var interval: String
            @Binding var selfShown: Bool
            @State var selectedQuantity = 7
            @State var selectedUnitIndex = 0
            
            private let intervalUnits = [
                "Days",
                "Months",
                "Years"
            ]
            
            private let defaultSelectedQuantity = 7
            private let defaultSelectedUnitIndex = 0
            
            var selectedUnitAsString: String {
                return "\(self.intervalUnits[self.selectedUnitIndex])"
            }
            
            init(interval: Binding<String>, selfShown: Binding<Bool>) {
                self._interval = interval
                self._selfShown = selfShown
                
                
                let components = interval.wrappedValue.components(separatedBy: " ")
                
                if components.count != 2 {
                    self._selectedQuantity = State(initialValue: self.defaultSelectedQuantity)
                    self._selectedUnitIndex = State(initialValue: self.defaultSelectedUnitIndex)
                } else {
                    self._selectedQuantity = State(initialValue: Int(components[0]) ?? self.defaultSelectedQuantity)
                    self._selectedUnitIndex = State(initialValue: self.intervalUnits.firstIndex(of: components[1]) ?? self.defaultSelectedUnitIndex)
                }
            }
            
            
            var body: some View {
                ZStack {
                    BackgroundImage(mode: .inverted)
                    VStack {
                        HStack {
                            Spacer()
                            ViewTitle("Viewing items expiring within...", color: Color.header)
                            Spacer()
                        }
                        
                        Spacer()
                        
                        
                        // MARK: EditIntervalSheet Picker
                        VStack {
                            GeometryReader { geoProxy in
                                HStack {
                                    Spacer()
                                    Text("\(self.selectedQuantity)")
                                    .font(.title)
                                    .fontWeight(.medium)
                                    
                                    Text(self.selectedUnitAsString)
                                    .font(.title)
                                    .fontWeight(.medium)
                                    
                                    Spacer()
                                }
                                .padding(.bottom)
                                
                                HStack {
                                    Picker(selection: self.$selectedQuantity, label: EmptyView()) {
                                        ForEach(0...100, id: \.self) {
                                            Text("\($0)")
                                        }
                                    }
                                    .frame(width: geoProxy.size.width / 2)
                                    
                                    Picker(selection: self.$selectedUnitIndex, label: EmptyView()) {
                                        ForEach(0..<self.intervalUnits.count) { index in
                                            Text("\(self.intervalUnits[index])")
                                        }
                                    }
                                    .frame(width: geoProxy.size.width / 2)
                                }
                                .offset(y: 30)
                                
                            }
                        }
                        .padding()
                        
                        Spacer()
                        
                        Button(action: setBtnClicked) {
                            Text("Set")
                                .fontWeight(.medium)
                                .foregroundColor(Color.white)
                                .padding()
                        }
                        .frame(minWidth: 100)
                        .background(Color.primary)
                        .cornerRadius(10)
                        .shadow(radius: 7)
                        
                        Spacer()
                    }
                    .padding()
                }
            }
            
            // MARK: EditIntervalSheet Methods
            
            func setBtnClicked() {
                interval = "\(selectedQuantity) \(selectedUnitAsString)"
                selfShown = false
            }
        }
    }
}

struct ExpiredItemsView_Previews: PreviewProvider {
    static var previews: some View {
        ExpiredItemsView()
    }
}
