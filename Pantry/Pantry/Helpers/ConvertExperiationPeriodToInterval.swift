//
//  ConvertExperiationPeriodToInterval.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation

let experiationPeriodConversionTable = [
    "day": 86400,
    "days": 86400,
    "month": 2.628e+6,
    "months": 2.628e+6,
    "year": 3.154e+7,
    "years": 3.154e+7
]

func converExperationPeriodToSeconds(experiationPeriod: String) -> Int {
    let pieces = experiationPeriod.components(separatedBy: " ")
    let unit = pieces[1].lowercased()
    let count = pieces[0]
    
    guard let formattedCount = Int(count) else {
        return 0
    }
    
    guard let formattedUnit =  experiationPeriodConversionTable[unit] else {
        return 0
    }
    
    return formattedCount * Int(formattedUnit)
}
