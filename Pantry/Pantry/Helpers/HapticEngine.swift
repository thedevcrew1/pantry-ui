//
//  HapticEngine.swift
//  Pantry
//
//  Created by Carson Franklin on 5/12/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//  Refrence: https://developer.apple.com/documentation/corehaptics/preparing_your_app_to_play_haptics
//  Refrence: https://www.hackingwithswift.com/example-code/core-haptics/how-to-play-custom-vibrations-using-core-haptics
//  Refrence: https://www.hackingwithswift.com/books/ios-swiftui/making-vibrations-with-uinotificationfeedbackgenerator-and-core-haptics

import Foundation
import CoreHaptics
import SwiftUI
 

class HapticEngine {
    private var engine: CHHapticEngine?
    
    static var shared = HapticEngine()
    
    private init() {
        guard CHHapticEngine.capabilitiesForHardware().supportsHaptics else { return }
        
        do {
            self.engine = try CHHapticEngine()
            engine?.resetHandler = resetHandler
            try engine?.start()
        } catch {
            print("There was an error creating the haptic engine: \(error.localizedDescription)")
        }
    }
    
    // Used to reset the engine.
    func resetHandler() {
        do {
            try self.engine?.start()
        } catch {
            print("There was an error reseting the Haptic Engine: \(error.localizedDescription)")
        }
    }

    /// A haptic to be played on a select event.
    func selectHaptic() {
        // Check if device supports haptics
        guard CHHapticEngine.capabilitiesForHardware().supportsHaptics else { return }
        
        var events = [CHHapticEvent]()
        
        let intensity = CHHapticEventParameter(parameterID: .hapticIntensity, value: 1)
        let sharpness = CHHapticEventParameter(parameterID: .hapticSharpness, value: 1)
        
        let event = CHHapticEvent(eventType: .hapticTransient, parameters: [intensity, sharpness], relativeTime: 0)
        events.append(event)
        
        do {
            let pattern = try CHHapticPattern(events: events, parameters: [])
            let player = try engine?.makePlayer(with: pattern)
            try player?.start(atTime: 0)
        } catch {
            print("Failed to play haptic pattern: \(error.localizedDescription).")
            resetHandler()
        }
    }
    
    /// A haptic to be played on a succesful event.
    func successHaptic() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
    
    /// A haptic to be played when an error occurs.
    func errorHaptic() {
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
}
