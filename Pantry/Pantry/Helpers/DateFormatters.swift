//
//  DateFormatters.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation

class AppDateFormatter {
    
    let serverDateFormatter: DateFormatter
    let appDateFormatter: DateFormatter
    
    init() {
        let serverDateFormatter = DateFormatter()
        serverDateFormatter.dateFormat = AppSettings.serverDateFormat
        self.serverDateFormatter = serverDateFormatter
        
        let appDateFormatter = DateFormatter()
        appDateFormatter.dateFormat = AppSettings.appDateFormat
        self.appDateFormatter = appDateFormatter
    }

    func formatAsServerDate(_ date: Date) -> String {
        return serverDateFormatter.string(from: date)
    }

    func dateFromServerDate(_ date: String) -> Date {
        return serverDateFormatter.date(from: date)!
    }

    func formatAsAppDate(_ date: Date) -> String {
        return appDateFormatter.string(from: date)
    }

    func dateFromAppDate(_ date: String) -> Date {
        return appDateFormatter.date(from: date)!
    }
}

let appDateFormatter = AppDateFormatter()
