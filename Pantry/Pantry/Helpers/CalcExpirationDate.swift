//
//  CalcExpirationDate.swift
//  Pantry
//
//  Created by Carson Franklin on 7/16/20.
//  Copyright © 2020 Carson Franklin. All rights reserved.
//

import Foundation


func calcExpirationDate(expirationPeriod: String) -> Date {
    return Date() + TimeInterval(converExperationPeriodToSeconds(experiationPeriod: expirationPeriod))
}
