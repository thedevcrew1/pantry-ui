# Pantry UI
The purpose of this repo Is to house the code for the pantry app's UI.

# Installation
* Run `$pod install`, to Install the dependencies and create your workspace.

# Directory Structure
* Navigation
	* Files pertaining to the custom navigation functionality.
* Config
	* Configuration for the UI (i.e. the base_url).
* Services
	* Contains classes pertaining to the app logic/ business logic of the application.
* Styles
	* Contains files associated with common/ global styles of the application.
* Network
	* Contains code nesessary to communicate accross the "wire."
* Data
	* Contains the data models for the application. Also, contians the store. The store is where we house the majority of the app data for a given session.
* Components
	* Small custom views used to build larger/ fuller views.
* Views
	* These are the idividual screens the user sees. Often these are built from combinations of components.
